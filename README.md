# OPTIMAL STATE #

## README ##



### To Compile ###

* Ensure that Android Studio SDK's for google services are installed and updated
* In Android Studio go to Tools -> SDK manager -> SDK tools -> make sure google play services and android SDK build tools are installed and updated
* Runs on android phones and tablets. 

### Firebase ###

* Gradle dependencies for firebase services must all have same version number
* ex: implementation 'com.google.firebase:firebase-core:11.8.0'
      implementation 'com.google.firebase:firebase-auth:11.8.0'
      
### Test Accounts ###
The following accounts are used for system testing. Id's are provided so that adding and removing clients can be done
without having to log into a users account to obtain their ID

user1@yahoo.com           
User1!                
Wxs4ghcEyuROI9C2vlBu0TiDgfp2

user2@yahoo.com           
User2!                
eBFEKACf8tgNom4bYL7ItUFBrYB3

user3@yahoo.com             
User3!                  
iCWNE49cVmfWTthTrK0CCW301kH3

user4@yahoo.com             
User4!                  
MoCZk6HzVCOWyz5ANijcMv8ABKo1

user5@yahoo.com             
User5!                  
QVFvvWhYdLWXYHyLsCVw6taZNbu1

provider1@yahoo.com         
Provider1!              
rWM9rtPpsyUQqRQxT17EnQ7KpjB3

provider2@yahoo.com         
Provider2!              
lyX585xkJ5enOAITsy0W0Qs82Ij2

