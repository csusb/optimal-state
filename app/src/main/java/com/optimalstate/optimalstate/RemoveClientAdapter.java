package com.optimalstate.optimalstate;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RemoveClientAdapter extends ArrayAdapter<Clients> {

    private List<Clients> clientsList;
    private Context context;


    public RemoveClientAdapter(List<Clients> clientsList, Context context) {
        super(context, R.layout.client_adapter_layout, clientsList);
        this.context = context;
        this.clientsList = clientsList;
    }

    private static class ClientHolder{
        public TextView tvClientName;
        public CheckBox removeSelected;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        ClientHolder holder = new ClientHolder();

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.client_adapter_layout, null);

            holder.tvClientName  = (TextView) convertView.findViewById(R.id.tvClientName);
            holder.removeSelected= (CheckBox) convertView.findViewById(R.id.removeCheckBox);

            holder.removeSelected.setOnCheckedChangeListener((ProviderRemoveClient) context);
        }
        else{
            holder = (ClientHolder) convertView.getTag();
        }

        Clients c = clientsList.get(position);

        if(Objects.equals(c.getView(), "true")){
            holder.tvClientName.setText(c.getFirst() + " " + c.getLast());
        }

        else
            holder.tvClientName.setText("no data");

        holder.removeSelected.setChecked(c.isSelected());
        holder.removeSelected.setTag(c);

        return convertView;

    }
}
