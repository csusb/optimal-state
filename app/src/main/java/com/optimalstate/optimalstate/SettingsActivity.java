/***************************************************************************************
 * SettingsActivity
 *
 * This Activity allows a user to change settings for their account. The main
 * Settings a user may change is personal information which they provided during
 * registration, set notification times, share with other users, or a user may
 * choose to log out of account
 **************************************************************************************/
package com.optimalstate.optimalstate;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Objects;

/*
This class implements OnClickListener which is defined at the end of this section.
OnClickListener performs the actions that are exucuted when a button is pressed
*/

public class SettingsActivity extends Activity implements View.OnClickListener {

    //Initializing SettingsActity.xml layouts
    private Intent intent;
    private Button userSettingsButton;
    private Button notificationSettingsButton;
    private Button shareWithButton;
    private Button backbutton;
    private String provider;

    /*
    This function is executed as soon as activity loads
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        try{
            //Retrive information from intent for is user a provider
            Bundle bundle = getIntent().getExtras();
            provider = bundle.getString("provider");

            //assign layouts to variables
            userSettingsButton = (Button) findViewById(R.id.userSettingsButton);
            notificationSettingsButton = (Button) findViewById(R.id.notificationSettingsButton);
            shareWithButton = (Button) findViewById(R.id.shareWithButton);
            backbutton = (Button) findViewById(R.id.backbutton);

            if(Objects.equals(provider,"Yes")) {
                shareWithButton.setVisibility(View.INVISIBLE);
                notificationSettingsButton.setVisibility(View.INVISIBLE);
            }
        }
        catch(NullPointerException e) {
            Log.e("Settings", e.getMessage());
        }
        catch(Exception e) {
            Log.e("Settings", e.getMessage());
        }

        //set OnClickListener to layouts
        userSettingsButton.setOnClickListener(this);
        notificationSettingsButton.setOnClickListener(this);
        shareWithButton.setOnClickListener(this);
        backbutton.setOnClickListener(this);
    }

    /*
    If a OnClickListener is clicked, a switch case is used to figure which was called based on R.id.
    Each will take user to another activity
    */
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.userSettingsButton:
                try{
                    finish();
                    intent =new Intent(this,ChangeUserInfo.class);
                    intent.putExtra("provider",provider);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            case R.id.notificationSettingsButton:
                break;
            case R.id.shareWithButton:
                try{
                    finish();
                    intent = new Intent(this,ShareWith.class);
                    intent.putExtra("provider",provider);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            case R.id.backbutton:
                if(Objects.equals(provider,"Yes")) {
                    try{
                        finish();
                        intent = new Intent(SettingsActivity.this, ProviderMain.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        break;
                    }
                    catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                } else{
                    try{
                        finish();
                        intent = new Intent(SettingsActivity.this, UserMain.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        break;
                    }
                    catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                    }
       }
    }
}
