/********************************************************************************************
 * ViewHistory
 *
 * This class allows a user to view their history from any date. The history will
 * be presented in a list view which includes colored images which represent their
 * choice during that assessment. If no data was found for the given day, "no
 * data" will be displayed to the user
 *******************************************************************************************/

package com.optimalstate.optimalstate;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.Calendar;

public class ViewHistory extends Activity implements View.OnClickListener{

    //variables
    private Intent intent;
    private Button backButton;
    private ArrayList<History> historyList = new ArrayList<>();
    private ListView list;
    private ScrollView scroll;
    private TextView dateSelecter;
    private DatePickerDialog.OnDateSetListener mOnDateSetListener;
    private String date;
    private String dateSearch;
    private ArrayList<History> nullHistory = new ArrayList<>();
    private ImageView help;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_history);

        try{
            //assign variables to views
            backButton = (Button) findViewById(R.id.backbutton);
            list = (ListView) findViewById(R.id.historyView);
            scroll = (ScrollView) findViewById(R.id.scroll);
            dateSelecter = (TextView) findViewById(R.id.dateSelecter);
            help = (ImageView) findViewById(R.id.help_History);
        }
        catch(NullPointerException e) {
            Log.e("ViewHistory", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ViewHistory", e.getMessage());
        }

        //set onClickListeners
        dateSelecter.setOnClickListener(this);

        //get the starting date
        getDate();

        //Set onDateSelectListener. Will update dateSearch with new date if one is selected.
        //Date is formated to match the database.Get data is then called to update the Listview
        mOnDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                try{
                    month = month + 1;
                    date = month + "/" + dayOfMonth + "/" + year;
                    dateSelecter.setText(date);
                    if(dayOfMonth < 10 && month < 10)
                        dateSearch = (year + "-" + "0" + month + "-" + "0"+ dayOfMonth).toString();
                    else if(dayOfMonth < 10)
                        dateSearch = (year + "-" + month + "-" + "0"+ dayOfMonth).toString();
                    else if(month < 10)
                        dateSearch = (year + "-" + "0" + month + "-" + dayOfMonth).toString();
                    else
                        dateSearch = (year + "-" + month + "-" + dayOfMonth).toString();

                    getData();
                }
                catch(NullPointerException e) {
                    Log.e("ViewHistory", e.getMessage());
                }
                catch(Exception e) {
                    Log.e("ViewHistory", e.getMessage());
                }
            }
        };

        //Set the default listView to null data
        nullData();

        //Get data to update listView
        getData();

        //set onclickListeners
        help.setOnClickListener(this);
        backButton.setOnClickListener(this);

        //This listener allows for a nested scrollView. If selected inside listView, listView will
        //scroll. Else the entire page will scroll
        list.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                scroll.requestDisallowInterceptTouchEvent(true);

                int action = event.getActionMasked();

                switch (action) {
                    case MotionEvent.ACTION_UP:
                        scroll.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });
    }

    //perform given action when clicked
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //Starts the User activity activity
            case R.id.backbutton:
                try{
                    finish();
                    intent = new Intent(this, UserMain.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            //Opens a window for the dateSelector. Allows users to select day, then the information
            //is saved to a variable to be displayed on screen
            case R.id.dateSelecter:
                try{
                    Calendar cal = Calendar.getInstance();
                    int year = cal.get(Calendar.YEAR);
                    int month = cal.get(Calendar.MONTH);
                    int day = cal.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog dialog = new DatePickerDialog(
                            ViewHistory.this,android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                            mOnDateSetListener,year,month,day);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                    break;
                }
                catch(NullPointerException e) {
                    Log.e("ViewHistory", e.getMessage());
                }
                catch(Exception e) {
                    Log.e("ViewHistory", e.getMessage());
                }
            //Opens help Activity if the user needs help. Opens a pop up window
            case R.id.help_History:
                try{
                    String help = getString(R.string.helpHistory);
                    intent = new Intent(this, HelpPopup.class);
                    intent.putExtra("help_text", help);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
        }
    }

    //This function updates the listView when new dates are selected
    public void updateList(ArrayList<History> newList){
        try{
            list.setAdapter(null);
            HistoryListAdapter adapter = new HistoryListAdapter(this,R.layout.history_adapter_layout, newList);
            list.setAdapter(adapter);
        }
        catch(NullPointerException e) {
            Log.e("ViewHistory", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ViewHistory", e.getMessage());
        }
    }

    //This function gets data from the database for users history. It uses the dateSearch variable to search for
    //the given dates. If there is not history, nullHistory is called, else updateList is called.
    public void getData(){
        FirebaseUtil.getRefdatabase().addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    DataSnapshot assessments = dataSnapshot.child(FirebaseUtil.getUserId()).child("Assessments").child(dateSearch);
                    Iterable<DataSnapshot> children = assessments.getChildren();
                    historyList.clear();
                    for (DataSnapshot postSnapshot: children){
                        History history = postSnapshot.getValue(History.class);
                        historyList.add(history);
                    }
                    if(historyList.isEmpty())
                        nullData();
                    else
                        updateList(historyList);
                }
                catch(DatabaseException UNKNOWN_ERROR){
                    Log.e("database",UNKNOWN_ERROR.getMessage());
                }
            }

            //Log error if failed to get data
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(databaseError.getDetails(),"Error getting data");
            }
        });
    }

    //This function gets the date selected and formates it to the correct format. The dateSelector
    //view is then updated with new date
    public void getDate(){
        try{
            Calendar cal = Calendar.getInstance();
            int day = cal.get(Calendar.DAY_OF_MONTH);
            int month = cal.get(Calendar.MONTH);
            int year = cal.get(Calendar.YEAR);
            month = month +1;
            if(day < 10 && month < 10)
                dateSearch = (year + "-" + "0" + month + "-" + "0"+ day).toString();
            else if(day < 10)
                dateSearch = (year + "-" + month + "-" + "0"+ day).toString();
            else if(month < 10)
                dateSearch = (year + "-" + "0" + month + "-" + day).toString();
            else
                dateSearch = (year + "-" + month + "-" + day).toString();
            date = month + "/" + day + "/" + year;
            dateSelecter.setText(date);
        }
        catch(NullPointerException e) {
            Log.e("ViewHistory", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ViewHistory", e.getMessage());
        }
    }

    //This function creates a listView That will display "No Data"
    public void nullData(){
        try{
            nullHistory.clear();
            History noHistory = new History("","No Data","");
            nullHistory.add(noHistory);
            HistoryListAdapter adapter = new HistoryListAdapter(this,R.layout.history_adapter_layout, nullHistory);
            list.setAdapter(adapter);
        }
        catch(NullPointerException e) {
            Log.e("ViewHistory", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ViewHistory", e.getMessage());
        }
    }
}
