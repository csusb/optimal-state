/********************************************************************************************
 * Clients
 *
 * This class holds information retrieved from the firebase database. It holds a clients
 * first and list name, their userId and status of their viewability. functions is this class
 * are getters and setters and toString which returns the users first and last name
 *******************************************************************************************/

package com.optimalstate.optimalstate;

public class Clients {

    //class variables
    private boolean isSelected;
    private String id;
    private String first;
    private String last;
    private String view;
    private String blocked;

    //default constructor, no parameters
    public Clients()
    { isSelected = false;}

    //default constructor, with parameters
    public Clients(String id, String first, String last, String view, String blocked) {
        this.id = id;
        this.first = first;
        this.last = last;
        this.view = view;
        this.blocked = blocked;
        isSelected = false;
    }

    //returns user id
    public String getId() {
        return id;
    }

    //sets user id
    public void setId(String id) {
        this.id = id;
    }

    //returns user first name
    public String getFirst() {
        return first;
    }

    //sets user first name
    public void setFirst(String first) {
        this.first = first;
    }

    //returns user last name
    public String getLast() {
        return last;
    }

    //sets user last name
    public void setLast(String last) {
        this.last = last;
    }

    //returns user viewability
    public String getView() {
        return view;
    }

    //sets user viewability
    public void setView(String view) {
        this.view = view;
    }
    
    //set user selected state
    public boolean isSelected() { return isSelected; }
    public void setSelected(boolean selected) { isSelected = selected; }

    //set blocked status
    public String getBlocked() { return blocked; }

    //get blocked status
    public void setBlocked(String blocked) { this.blocked = blocked; }

    //returns first and last name together as single string
    @Override
    public String toString(){
        return first + " " + last;
    }
}
