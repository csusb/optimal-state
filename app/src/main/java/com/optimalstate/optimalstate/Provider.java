/*******************************************************************************************
 * Provider
 *
 * This class holds the variables of a provider. It contains strings for first, last, id
 * and view. A boolean isSelected is for listViews that contain checkboxs. By default
 * isSelected is set to false. Class includes getters and setters for each variable.
 ******************************************************************************************/

package com.optimalstate.optimalstate;

public class Provider {

    //variables
    private boolean isSelected;
    private String first;
    private String last;
    private String id;
    private String view;
    private String blocked;

    //default constructor, no parameters
    public Provider()
    {
        isSelected = false;
    }

    //default constructor, with parameters
    public Provider(String first, String last, String id, String view, String blocked) {
        this.first = first;
        this.last = last;
        this.id = id;
        this.view = view;
        this.blocked = blocked;
        isSelected = false;
    }

    //return first name
    public String getFirst() {
        return first;
    }

    //set first name
    public void setFirst(String first) {
        this.first = first;
    }

    //return last name
    public String getLast() {
        return last;
    }

    //set last name
    public void setLast(String last) {
        this.last = last;
    }

    //return user id
    public String getId() { return id; }

    //set user id
    public void setId(String id) {
        this.id = id;
    }

    //return view type
    public String getView() {
        return view;
    }

    //set view type
    public void setView(String view) {
        this.view = view;
    }

    //returns boolean if selected
    public boolean isSelected() {
        return isSelected;
    }

    //get blocked status
    public String getBlocked() { return blocked; }

    //set blocked status
    public void setBlocked(String blocked) { this.blocked = blocked; }

    //set a change in boolean if selected
    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
