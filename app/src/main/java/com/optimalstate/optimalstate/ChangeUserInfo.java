/**********************************************************************************************
 * ChangeUserInfo
 *
 * This activity provides users the ability to update and change their information, all information 
 * which is changed or user tries to change will go through many checks to ensure that information 
 * is valid. 
 **********************************************************************************************/

package com.optimalstate.optimalstate;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.ValueEventListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ChangeUserInfo extends Activity implements View.OnClickListener {

    //Initializing activity_change_user_info.xml layouts
    private EditText newFirstName;
    private EditText newLastName;
    private EditText newWeight;

    //will be used in later implementations
    //private EditText newHeightFT;
    //private EditText newHeightIN;

    private Button CommitChangesButton;
    private Button BackButton;
    private Button ResetPasswordButton;
    private String databaseFirst;
    private String databaseLast;
    private String databaseWeight;
    private String provider;

    //Sets all variables to their corresponding layouts
    @Override
    public void  onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_user_info);

        try{
        Bundle bundle = getIntent().getExtras();
        provider = bundle.getString("provider");

        //this prevents the keyboard from loading on start up
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        //Assign layouts to variables
            newFirstName = (EditText) findViewById(R.id.etChangeFirstName);
            newLastName = (EditText) findViewById(R.id.etChangeLastName);
            newWeight = (EditText) findViewById(R.id.etChangeWeight);
            CommitChangesButton = (Button) findViewById(R.id.btnCommitChanges);
            BackButton = (Button) findViewById(R.id.btnBack);
            ResetPasswordButton = (Button) findViewById(R.id.btnChangePassword);
        }
        catch(NullPointerException e) {
            Log.e("ChangeUserInfo", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ChangeUserInfo", e.getMessage());
        }
        //will be used in later implementations
        //private EditText newHeightFT;
        //private EditText newHeightIN;

        //Retrieve users first and last name along with their assessment status.
        FirebaseUtil.getRefdatabase().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    databaseFirst = dataSnapshot.child(FirebaseUtil.getUserId()).child("UserInfo").child("first").getValue(String.class);
                    databaseLast = dataSnapshot.child(FirebaseUtil.getUserId()).child("UserInfo").child("last").getValue(String.class);
                    databaseWeight = dataSnapshot.child(FirebaseUtil.getUserId()).child("UserInfo").child("weight").getValue(String.class);
                    newFirstName.setHint(databaseFirst);
                    newLastName.setHint(databaseLast);
                    newWeight.setHint(databaseWeight);
                }
                catch(DatabaseException UNKNOWN_ERROR){
                    Log.e("database",UNKNOWN_ERROR.getMessage());
                }
                catch(NullPointerException e) {
                Log.e("Text view hints", e.getMessage());
            }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(databaseError.getDetails(),"Error reading from database");
            }
        });

        //Set OnClickListeners to layouts that are clickable
        CommitChangesButton.setOnClickListener(this);
        BackButton.setOnClickListener(this);
        ResetPasswordButton.setOnClickListener(this);
    }


    //Function which does logic on what information was input to change
    private void SaveChanges() {
        try {
            String firstName = newFirstName.getText().toString().trim();
            String lastName = newLastName.getText().toString().trim();
            String weight = newWeight.getText().toString().trim();
            int intAge = 0;
            String date = new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date());
            int intDate = Integer.parseInt(date);

        //will be used in later implementations
        //String feet = newHeightFT.getText().toString().trim();
        //String inches = newHeightIN.getText().toString().trim();

        //Checks if the edit text box is empty for first name, if not empty then if function is executed
        if(!firstName.isEmpty()) {

            //This if statement checks if the string only contains letters and nothing else.
            if(firstName.matches(".*[0-9]+.*")) {
                newFirstName.setError("Only letters are allowed!");
                newFirstName.requestFocus();
                return;
            }

            //Checks if the character is larger or equal to 24 characters
            if(firstName.length() >= 24){
                newFirstName.setError("Name is to long..");
                newFirstName.requestFocus();
                return;
            }

            //Update information in Firebase database and notify user of success
            try{
                FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("UserInfo").child("first").setValue(firstName);
                Toast.makeText(ChangeUserInfo.this, "Successfully Changed First Name", Toast.LENGTH_LONG).show();
            }
            catch(DatabaseException UNKNOWN_ERROR){
                Log.e("database",UNKNOWN_ERROR.getMessage());
            }
        }

        //Checks if last name edit text box is empty or not
        if(!lastName.isEmpty()){

            //This if{} statement checks if the string only contains letters and nothing else
            if(lastName.matches(".*[0-9]+.*")){
                newLastName.setError("Only letters are allowed!");
                newLastName.requestFocus();
                return;
            }

            //Checks if string is larger or equal to 24 characters
            if(lastName.length() >= 24){
                newLastName.setError("Name is to long..");
                newLastName.requestFocus();
                return;
            }

            //Update information in Firebase database and notify user of success
            try{
            FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("UserInfo").child("last").setValue(lastName);
            Toast.makeText(ChangeUserInfo.this, "Successfully Changed Last Name", Toast.LENGTH_LONG).show();
            }
            catch(DatabaseException UNKNOWN_ERROR){
                Log.e("database",UNKNOWN_ERROR.getMessage());
            }
        }


        /*
         * Checks if new weight edit text box is empty, if not empty then it will execute a set of if
         * function checks, this is done to ensure that information that is input is valid and logical.
         * It checks if weight length can not be greater than 3.
         */
        if(!weight.isEmpty()){

            if(weight.length() >= 4) {
                newWeight.setError("Invalid weight");
                newWeight.requestFocus();
                return;
            }
            //Update information in Firebase database and notify user of success
            try{
            FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("UserInfo").child("weight").setValue(weight);
            Toast.makeText(ChangeUserInfo.this, "Successfully Changed Weight", Toast.LENGTH_LONG).show();
            }
            catch(DatabaseException UNKNOWN_ERROR){
                Log.e("database",UNKNOWN_ERROR.getMessage());
            }
        }

       /*
        * will be used in later implementations
        * Checks if Height boxes have input or not, checks both feet and inches
        * if either feet or inches have an input then an if statement will be executed
        * which checks if the input is a valid input which fits between the boundaries we have setup
        * this ensures that someone cannot input a height in feet below 3ft or above 7ft
        * as well as ensures that someone cannot input a height in inches below 0in or above 12in,
        * since all four of those scenarios are invalid and illogical

        if(!feet.isEmpty()){
           if(feet <= 3 && feet >= 7){
               newHeightFT.setError("invalid");
               newHeightFT.requestFocus();
               break;
           }


             * The following line of code is what is responsible of going into database and updating users height in feet
             *
             * database.child(userID).child("feet").setValue(feet);


       }

        //will be used in later implementations
        if (!inches.isEmpty()){
           if (inches < 0 && inches >=12) {
               newHeightIN.setError("Invalid");
               newHeightIN.requestFocus();
               break;
           }


             * The following line of code is what is responsible of going into database and updating users height in feet
             *
             * database.child(userID).child("inches").setValue(inches);
             *
        }*/
        }
        catch(NullPointerException e) {
            Log.e("ChangeUserInfo", e.getMessage());
        }
    }

    //Function which allows user to go back to previous interface
    private void Back() {
        try{
            Intent intent = new Intent(ChangeUserInfo.this, SettingsActivity.class);
            intent.putExtra("provider",provider);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }


    //Function which allows users to Change their passwords
    private void resetPassword(){
        String emailAddress = FirebaseUtil.getUser().getEmail();
        FirebaseUtil.getFirebaseAuth().sendPasswordResetEmail(emailAddress).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    try {
                        Toast.makeText(ChangeUserInfo.this, "Password Reset email sent!", Toast.LENGTH_SHORT).show();
                        finish();
                        startActivity(new Intent(ChangeUserInfo.this, LoginActivity.class));
                    }
                    catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(ChangeUserInfo.this, "Error in sending Password reset email!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //performs action based on button clicked
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCommitChanges:
                SaveChanges();
                break;
            case R.id.btnBack:
                Back();
                break;
            case R.id.btnChangePassword:
                resetPassword();
                break;
        }
    }
}
