/****************************************************************************************
 *  Register User
 *
 * This Activity is only accessed from the LoginActivity. This Activity is used to gather
 * information from the user so that they can create an acccount which stores data on the
 * Firebase database. Once registration is complete, the user will be taken to their
 * UserMainActivity. If they have an account they may select already registered to return
 * to the LoginActivity
 ***************************************************************************************/

    package com.optimalstate.optimalstate;

    import android.app.Activity;
    import android.app.ProgressDialog;
    import android.content.ActivityNotFoundException;
    import android.content.Intent;
    import android.graphics.Color;
    import android.os.Bundle;
    import android.support.annotation.NonNull;
    import android.util.Log;
    import android.util.Patterns;
    import android.view.View;
    import android.view.WindowManager;
    import android.widget.AdapterView;
    import android.widget.ArrayAdapter;
    import android.widget.Button;
    import android.widget.EditText;
    import android.widget.ImageView;
    import android.widget.Spinner;
    import android.widget.TextView;
    import android.widget.Toast;
    import com.google.android.gms.tasks.OnCompleteListener;
    import com.google.android.gms.tasks.Task;
    import com.google.firebase.auth.AuthResult;
    import com.google.firebase.auth.FirebaseAuthUserCollisionException;
    import com.google.firebase.database.DatabaseException;

    import java.text.SimpleDateFormat;
    import java.util.Date;
    import java.util.Locale;
    import java.util.Objects;
    import java.util.regex.Matcher;
    import java.util.regex.Pattern;

    /*
    This class implements OnClickListener which is defined at the end of this section.
    OnClickListener performs the actions that are exucuted when a button is pressed
    */
    public class RegisterUser extends Activity implements View.OnClickListener{

    //Initializing RegisterUser.xml layouts
    private Intent intent;
    private Button buttonRegister;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextComfirmPassword;
    private TextView textViewSignIn;
    private EditText textUserFirstName;
    private EditText textUserLastName;
    private EditText editTextDay;
    private EditText editTextMonth;
    private EditText editTextYear;
    private EditText editWeight;
    private Pattern pattern;
    private Matcher matcher;
    private static final String PASSWORD_PATTER = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{6,20})";
    private ImageView help;

    //Initializing Spinners
    private Spinner genderSpinner;
    ArrayAdapter<CharSequence> genderAdapter;
    private Spinner ethnicitySpinner;
    ArrayAdapter<CharSequence> ethnicityAdapter;
    private Spinner providerSpinner;
    ArrayAdapter<CharSequence> providerAdapter;

    //Initializing a progress bar which can display messages when action is taken
    private ProgressDialog progressDialog;

    //Variables to hold spinner selection
    private String gender;
    private String ethnicity;
    private String provider;

    /*
     This function is executed as soon as activity loads
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        //This prevents the keyboard from starting up on start up
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //Create instance of a progress bar
        progressDialog = new ProgressDialog(this);

        //assign layouts to variables
        try{
        buttonRegister = (Button) findViewById(R.id.buttonRegister);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextComfirmPassword = (EditText) findViewById(R.id.editTextPasswordConfirm);
        textUserFirstName = (EditText) findViewById(R.id.textUserFirstName);
        textUserLastName = (EditText) findViewById(R.id.textUserLastName);
        textViewSignIn = (TextView) findViewById(R.id.textViewSingIn);
        editTextDay = (EditText) findViewById(R.id.editTextDay);
        editTextMonth = (EditText) findViewById(R.id.editTextMonth);
        editTextYear = (EditText) findViewById(R.id.editTextYear);
        editWeight = (EditText) findViewById(R.id.editTextWeight);
        help = (ImageView) findViewById(R.id.helpRegister);

        //assign spinners, setting adapter, and onItemSelect
        genderSpinner = (Spinner) findViewById(R.id.genderSpinner);
        genderAdapter = ArrayAdapter.createFromResource(this, R.array.gender,android.R.layout.simple_spinner_item);
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(genderAdapter);
        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int posistion, long id) {
                //store value that was selected
                try{
                gender = genderSpinner.getSelectedItem().toString();
                }
                catch(NullPointerException e) {
                    Log.e("RegisterUser", e.getMessage());
                }
                catch(Exception e) {
                    Log.e("RegisterUser", e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //does nothing but is required declared
            }
        });

        //assign spinners, setting adapter, and onItemSelect
        ethnicitySpinner = (Spinner) findViewById(R.id.ethnicitySpinner);
        ethnicityAdapter = ArrayAdapter.createFromResource(this, R.array.ethicity,android.R.layout.simple_spinner_item);
        ethnicityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ethnicitySpinner.setAdapter(ethnicityAdapter);
        ethnicitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int posistion, long id) {
                //store value that was selected
                try{
                ethnicity = ethnicitySpinner.getSelectedItem().toString();
                }
                catch(NullPointerException e) {
                    Log.e("RegisterUser", e.getMessage());
                }
                catch(Exception e) {
                    Log.e("RegisterUser", e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //does nothing but is required declared
            }
        });

        //assign spinners, setting adapter, and onItemSelect
        providerSpinner = (Spinner) findViewById(R.id.providerSpinner);
        providerAdapter = ArrayAdapter.createFromResource(this, R.array.provider,android.R.layout.simple_spinner_item);
        providerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        providerSpinner.setAdapter(providerAdapter);
        providerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int posistion, long id) {
                //Store value that was selected
                try{
                provider = providerSpinner.getSelectedItem().toString();
                }
                catch(NullPointerException e) {
                    Log.e("RegisterUser", e.getMessage());
                }
                catch(Exception e) {
                    Log.e("RegisterUser", e.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //does noting but is required declared
            }
        });

        }
        catch(NullPointerException e) {
            Log.e("RegisterUser", e.getMessage());
        }
        catch(Exception e) {
            Log.e("RegisterUser", e.getMessage());
        }

        //Set OnClickListeners to layouts that are clickable
        help.setOnClickListener(this);
        buttonRegister.setOnClickListener(this);
        textViewSignIn.setOnClickListener(this);
    }

    /*
    This function validates user input for each textbox. If registration is sussessful
    user will be taken to their mainActivity
     */
    private void registerUser() {

        //initialize strings to hold email,password,first and last name, day, month, year, and
        // weight. Convert each to string and trim.
        try{
        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        final String confirmPassord = editTextComfirmPassword.getText().toString().trim();
        final String first = textUserFirstName.getText().toString().trim();
        final String last = textUserLastName.getText().toString().trim();
        final String stringDay = editTextDay.getText().toString().trim();
        final String stringMonth = editTextMonth.getText().toString().trim();
        final String stringYear = editTextYear.getText().toString().trim();
        final String stringWeight = editWeight.getText().toString().trim();

        //used to convert string to ints
        int day = 0;
        int month = 0;
        int year = 0;
        int weight = 0;

        //if day,month,and year is not empty, convert and store as integers
        if(!stringDay.isEmpty())
        day = Integer.parseInt(stringDay);

        if(!stringMonth.isEmpty())
        month = Integer.parseInt(stringMonth);

        if(!stringYear.isEmpty())
        year = Integer.parseInt(stringYear);

        if(!stringWeight.isEmpty())
            weight = Integer.parseInt((stringWeight));

        //Validate User input is not empty, else display error and highlist text field
        if(first.isEmpty()){
            textUserFirstName.setError(("Please enter first name"));
            textUserFirstName.requestFocus();
            return;
        }

        //check if length of first name is greater than 24 characters
        if(first.length() >= 24){
            textUserFirstName.setError(("First name is too long"));
            textUserFirstName.requestFocus();
            return;
        }

        //This if statement checks if the string only contains letters and nothing else.
        if(first.matches(".*[0-9]+.*")) {
            textUserFirstName.setError("Only letters are allowed!");
            textUserFirstName.requestFocus();
            return;
        }

        //Validate User input is not empty, else display error and highlist text field
        if(last.isEmpty()){
            textUserLastName.setError("Please enter last name");
            textUserLastName.requestFocus();
            return;
        }

        //check is length of last name is greater than 24 characters
        if(last.length() >= 24){
            textUserLastName.setError("Last name is too long");
            textUserLastName.requestFocus();
            return;
        }

        //This if statement checks if the string only contains letters and nothing else.
        if(last.matches(".*[0-9]+.*")) {
            textUserLastName.setError("Only letters are allowed!");
            textUserLastName.requestFocus();
            return;
        }

        //Validate User input is not empty, else display error and highlist text field
        if(email.isEmpty()) {
            //empty
            editTextEmail.setError("Email is required");
            editTextEmail.requestFocus();
            return;
        }

        //checks if input ressembles a valid email. If not, display error and highlight text field
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            editTextEmail.setError("Please enter a valid email");
            editTextEmail.requestFocus();
            return;
        }

        //Validate User input is not empty, else display error and highlist text fiel
        if(password.isEmpty()) {
            //empty
            editTextPassword.setError("Password is required");
            editTextPassword.requestFocus();
            return;
        }

        //check if password contains whitespace
        if(password.contains(" ")){
            editTextPassword.setError("Cannot contain spaces");
            editTextPassword.requestFocus();
            return;
        }

        //set patter to Password Pattern then compare if password contains required characters
        pattern = Pattern.compile(PASSWORD_PATTER);
        if(!validate(password)){
            editTextPassword.setError("Length must be 6-20 char\n" +
                    "Must contain 1 lower case\nMust contain 1 upper case\n" +
                    "must contain one special char !@#$%^&*");
            editTextPassword.requestFocus();
            return;
        }

        //check if passowrd and confirm password are equal
        if(!Objects.equals(password, confirmPassord)){
            editTextComfirmPassword.setError("Password does not match");
            editTextComfirmPassword.requestFocus();
            return;
        }

        //check if weight was entered
        if(stringWeight.isEmpty()){
            editWeight.setError("Please enter weight");
            editWeight.requestFocus();
            return;
        }

        //Check if weight is within a valid range
        if(weight <= 35 || weight > 500){
            editWeight.setError("Enter valid weight:\nEnter between 35-500");
            editWeight.requestFocus();
            return;
        }

        //check if user selected a gender
        if(Objects.equals(gender,"Select")) {
            TextView errorText = (TextView)genderSpinner.getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);
            errorText.setText("Select");
            genderSpinner.requestFocus();
            return;
        }

        //check if user selected a ethnicity
        if(Objects.equals(ethnicity,"Select")) {
            TextView errorText = (TextView)ethnicitySpinner.getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);
            errorText.setText("Select");
            genderSpinner.requestFocus();
            return;
        }

        //check if user selected a provider
        if(Objects.equals(provider,"Select")) {
            TextView errorText = (TextView)providerSpinner.getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);
            errorText.setText("Select");
            providerSpinner.requestFocus();
            return;
        }

        //check if day is empty
        if(stringDay.isEmpty()) {
            editTextDay.setError("input day");
            editTextDay.requestFocus();
            return;
        }

        //check if day is a valid number
        if(day <= 0 || day > 31){
            editTextDay.setError("invalid day");
            editTextDay.requestFocus();
            return;
        }

        //check if month is empty
        if(stringMonth.isEmpty()){
            editTextMonth.setError("input Month");
            editTextMonth.requestFocus();
            return;
        }

        //check if month is a valid number
        if(month <= 0 || month > 12){
            editTextMonth.setError("invalid Month");
            editTextMonth.requestFocus();
            return;
        }

        //check if year is empty
        if(stringYear.isEmpty()){
            editTextYear.setError("enter Year");
            editTextYear.requestFocus();
            return;
        }

        //gets current year and converts it to integer
        String date = new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date());
        int integerDate = Integer.parseInt(date);

        //checks if year is valid. currently validates between 1900 and current year
        if(year <= 1900 || year > integerDate){
            editTextYear.setError("Invalid Year");
            editTextYear.requestFocus();
            return;
        }

        //if validations passed, set message and display that logging in is being attempted
        progressDialog.setMessage("Registering User...");
        progressDialog.show();



        //This function creates the new users information in the database, assigning them a
        //unique userId. Users should never see their userId from the database.
        try{
            FirebaseUtil.setFirebaseAuth();
        }
        catch(DatabaseException UNKNOWN_ERROR){
            Log.e("database",UNKNOWN_ERROR.getMessage());
        }
        FirebaseUtil.getFirebaseAuth().createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        //if successful, set instance of firebase databasee, get instance of user and
                        //userId and a reference to the database.
                        if (task.isSuccessful()) {
                            try{
                                FirebaseUtil.setDatabase();
                                FirebaseUtil.setUser();
                                FirebaseUtil.setUserId();
                                FirebaseUtil.setRefdatabase();

                                //Store all valid inputs into the database and set default values
                                FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("UserInfo").child("first").setValue(first);
                                FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("UserInfo").child("last").setValue(last);
                                FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("UserInfo").child("DOB").child("day").setValue(stringDay);
                                FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("UserInfo").child("DOB").child("month").setValue(stringMonth);
                                FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("UserInfo").child("DOB").child("year").setValue(stringYear);
                                FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("UserInfo").child("gender").setValue(gender);
                                FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("UserInfo").child("ethnicity").setValue(ethnicity);
                                FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("UserInfo").child("weight").setValue(stringWeight);
                                FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("UserInfo").child("Provider").setValue(provider);
                                FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("PostAssessment").setValue("False");
                                FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Assessments").child("recent").setValue("0");

                                //toast success and start UserMain activity
                                Toast.makeText(RegisterUser.this, "Register Successful", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }
                            catch(DatabaseException UNKNOWN_ERROR){
                                Log.e("database",UNKNOWN_ERROR.getMessage());
                            }
                            if(Objects.equals(provider,"Yes")){
                                try{
                                    finish();
                                    intent = new Intent(RegisterUser.this, ProviderMain.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                                catch (ActivityNotFoundException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try{
                                    finish();
                                    intent = new Intent(RegisterUser.this, UserMain.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                                catch (ActivityNotFoundException e) {
                                    e.printStackTrace();
                                }
                            }

                        //If failed, display that they could not register. Later will add
                        // an exception message as to why registration failed
                        } else {
                            progressDialog.dismiss();
                            if(task.getException() instanceof FirebaseAuthUserCollisionException){
                                Toast.makeText(getApplicationContext(), "You are already registered", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
        }
        catch(NullPointerException e) {
            Log.e("RegisterUser", e.getMessage());
        }
        catch(Exception e) {
            Log.e("RegisterUser", e.getMessage());
        }
    }


    public boolean validate(final String password){
        matcher = pattern.matcher(password);
        return  matcher.matches();
    }

    /*
    If a OnClickListener is clicked, a switch case is used to figure which was called based on R.id.
    textViewSignIn will close this activity and restart LoginActivity. ButtonRegistier wil
    call registerUser() function above.
    */
    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.textViewSingIn:
                try{
                    finish();
                    intent = new Intent(this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            case R.id.buttonRegister:
                registerUser();
                break;
            case R.id.helpRegister:
                try{
                    String help = getString(R.string.helpRegister);
                    intent = new Intent(this, HelpPopup.class);
                    intent.putExtra("help_text", help);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
        }
    }}
