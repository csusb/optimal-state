/***************************************************************************************
 * HelpPopup
 *
 * This activity is a pop up window that provides information about the content on a
 * giving interface. The activity calling this class must pass a string through intent
 * in order to display text. clicking outside of the popup will close this activity.
 ***************************************************************************************/

package com.optimalstate.optimalstate;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.TextView;

public class HelpPopup extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pop_up);

        //Retrieve String that is passes in Intent
        try{
            Bundle bundle = getIntent().getExtras();
            String data = bundle.getString("help_text");

            //Set text view to string obtained from intent
            TextView textView = findViewById(R.id.help_text);
            textView.setText(data);
        }
        catch(NullPointerException e) {
            Log.e("Help Pop Up", e.getMessage());
        }
        catch(Exception e) {
            Log.e("Help Pop Up", e.getMessage());
        }
        //get size of device to resize the window
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;

        //set the layout for the window
        getWindow().setLayout((int)(width * .8),(int)(height * .6));
    }
}
