/*****************************************************************************************
 * MyAdapter
 *
 * This class provides the nessacarry functions to support a expandable list view. It
 * provides two main functions types which are for the main headers of the list view
 * and its direct children. This class only supports a two leveled list view, so another
 * ExpandableLevelAdapter is nested in the child part of this class.
 *****************************************************************************************/
package com.optimalstate.optimalstate;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class MyAdapter extends BaseExpandableListAdapter {

    //declaring variables
    String[] parentHeaders;
    List<String[]> secondLevel;
    private Context context;
    List<LinkedHashMap<String, String[]>> data;

    //constructor
    public MyAdapter(Context context, String[] parentHeader, List<String[]> secondLevel, List<LinkedHashMap<String, String[]>> data) {
        this.context = context;
        this.parentHeaders = parentHeader;
        this.secondLevel = secondLevel;
        this.data = data;
    }

    //returns parent group count
    @Override
    public int getGroupCount() {

        return parentHeaders.length;
    }

    //returns child group count. Because the child will be another adapter it will only return 1
    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    //return position of group object
    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    //return position of child object
    @Override
    public Object getChild(int group, int child) {
        return child;
    }

    //returns id of parent Object
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    //returns id of child object
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    //checks if Ids are good
    @Override
    public boolean hasStableIds() {
        return true;
    }

    //This function creates a inflater so that each parent can be expanded out. Each textView in
    //the xml file are set to header strings
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.parent_layout, null);
        TextView textView = (TextView) convertView.findViewById(R.id.parent_heading);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setText(this.parentHeaders[groupPosition]);
        return convertView;
    }

    //This function creates a second level with and additional expandable list view.
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        //instance of next expandableLIstView
        final SecondLevelExpandableListView secondLevelELV = new SecondLevelExpandableListView(context);

        //passing Level 2 headers
        String[] headers = secondLevel.get(groupPosition);
        List<String[]> childData = new ArrayList<>();
        HashMap<String, String[]> secondLevelData = data.get(groupPosition);

        //looping through and seting child data to second level
        for (String key : secondLevelData.keySet()) {
            childData.add(secondLevelData.get(key));
        }

        //setting expandable list view to adapter
        secondLevelELV.setAdapter(new SecondLevelAdapter(context, headers, childData));
        secondLevelELV.setGroupIndicator(null);

        //if another child is expanded, close previous expanded child
        secondLevelELV.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosistion) {
                if (groupPosistion != previousGroup)
                    secondLevelELV.collapseGroup(previousGroup);
                previousGroup = groupPosistion;
            }
        });
        return secondLevelELV;
    }

    //set each child to be selectable
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
