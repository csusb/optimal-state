/*************************************************************************************************
 * AssessmentActivity
 *
 * This activity allows the user to choose from four different section based on how they currently
 * feel. The user may select any combination of the three outside sections. If they select the
 * middle section the outside sections will automatically be deselected. The buttons of the
 * assessment are created by layering an invisible image on top of the background image. The
 * invisible image has three colors, red, white, blue, and yellow. When the user touches these
 * area's, the colorTool class will verify what they selected. After the background image will
 * be changed to another image that represents that section being selected. When the user submits
 * the value associated with that section is stored to the users database information. The user
 * is then taken to the Exercise activity.
 ************************************************************************************************/

package com.optimalstate.optimalstate;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class AssessmentActivity extends Activity implements View.OnTouchListener, View.OnClickListener {

    //declaring variables
    private Intent intent;
    private ImageView iv;
    private String assessmentValue;
    private Button backToUserMainBtn;
    private Button submitBtn;
    private String needPostAssessment;
    private ImageView help;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment);

        //initialize assesmentValue to 0
        assessmentValue = "0";

        //retrieve data from database and stores it in needPostAssessment
        FirebaseUtil.getRefdatabase().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    needPostAssessment = dataSnapshot.child(FirebaseUtil.getUserId()).child("PostAssessment").getValue(String.class);
                }
                catch(DatabaseException UNKNOWN_ERROR){
                    Log.e("database",UNKNOWN_ERROR.getMessage());
                }
            }

            //if failed to retrieve data, record error
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(databaseError.getDetails(), "Error retrieving data");
            }
        });

        //assigning id's to buttons
        try{
            backToUserMainBtn = findViewById(R.id.backToUserMainBtn);
            submitBtn = findViewById(R.id.submitBtn);
            iv = (ImageView) findViewById(R.id.image);
            help = (ImageView) findViewById(R.id.helpAssessment);

            //set on touch listeners if imageView is not null
            if (iv != null) {
                iv.setOnTouchListener(this);
            }
        }
        catch(NullPointerException e) {
            Log.e("Assessment", e.getMessage());
        }
        catch(Exception e) {
            Log.e("Assessment", e.getMessage());
        }

        //setting onClick listeners
        help.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
        backToUserMainBtn.setOnClickListener(this);
    }

    /**
     * Respond to the user touching the screen.
     * Change images to make things appear and disappear from the screen.
     */

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        //initialize a boolean value that will be returned
            boolean handledHere = false;

            //store action in veriable. This will be an touch up or down event
            final int action = event.getAction();

            //create and store values for x and y cordinates
            final int evX = (int) event.getX();
            final int evY = (int) event.getY();

            // resource id of the next image to display
            int nextImage = -1;

            // If we cannot find the imageView, return.
            ImageView imageView = (ImageView) v.findViewById(R.id.image);
            if (imageView == null) return false;

            // When the action is Down, see if we should show the "pressed" image for the default image.
            // We do this when the default image is showing. That condition is detectable by looking at the
            // tag of the view. If it is null or contains the resource number of the default image, display the pressed image.

            Integer tagNum = (Integer) imageView.getTag();
            int currentResource = (tagNum == null) ? R.drawable.background_main : tagNum.intValue();

            // Now that we know the current resource being displayed we can handle the DOWN and UP events.
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    if (currentResource == R.drawable.background_main) {
                        nextImage = R.drawable.background_main;
                        handledHere = true;
                    } else handledHere = true;
                    break;

                case MotionEvent.ACTION_UP:
                    // On the UP, we do the click action.
                    // The hidden image (image_areas) has three different hotspots on it.
                    // The colors are red, blue, and yellow.
                    // Use image_areas to determine which region the user touched.

                    int touchColor = getHotspotColor(R.id.image_areas, evX, evY);

                    // Compare the touchColor to the expected values. Switch to a different image, depending on what color was touched.
                    // Note that we use a Color Tool object to test whether the observed color is close enough to the real color to
                    // count as a match. We do this because colors on the screen do not match the map exactly because of scaling and
                    // varying pixel density.

                    ColorTool ct = new ColorTool();
                    int tolerance = 25;

                    //nothing selected yet selected
                    if (currentResource == R.drawable.background_main) {
                        if (ct.closeMatch(Color.RED, touchColor, tolerance)) {
                            nextImage = R.drawable.r_selected;
                            assessmentValue = "3";
                        } else if (ct.closeMatch(Color.BLUE, touchColor, tolerance)) {
                            nextImage = R.drawable.b_selected;
                            assessmentValue = "4";
                        } else if (ct.closeMatch(Color.WHITE, touchColor, tolerance)) {
                            nextImage = R.drawable.w_selected;
                            assessmentValue = "2";
                        } else if (ct.closeMatch(Color.YELLOW, touchColor, tolerance)) {
                            nextImage = R.drawable.g_selected;
                            assessmentValue = "1";
                        }
                    }

                    //white area already selected
                    if (currentResource == R.drawable.w_selected) {
                        if (ct.closeMatch(Color.RED, touchColor, tolerance)) {
                            nextImage = R.drawable.wr_selected;
                            assessmentValue = "5";
                        } else if (ct.closeMatch(Color.BLUE, touchColor, tolerance)) {
                            nextImage = R.drawable.wb_selected;
                            assessmentValue = "6";
                        } else if (ct.closeMatch(Color.WHITE, touchColor, tolerance)) {
                            nextImage = R.drawable.background_main;
                            assessmentValue = "0";
                        } else if (ct.closeMatch(Color.YELLOW, touchColor, tolerance)) {
                            nextImage = R.drawable.g_selected;
                            assessmentValue = "1";
                        }
                    }

                    //red area already selected
                    if (currentResource == R.drawable.r_selected) {
                        if (ct.closeMatch(Color.RED, touchColor, tolerance)) {
                            nextImage = R.drawable.background_main;
                            assessmentValue = "0";
                        } else if (ct.closeMatch(Color.BLUE, touchColor, tolerance)) {
                            nextImage = R.drawable.rb_selected;
                            assessmentValue = "8";
                        } else if (ct.closeMatch(Color.WHITE, touchColor, tolerance)) {
                            nextImage = R.drawable.wr_selected;
                            assessmentValue = "5";
                        } else if (ct.closeMatch(Color.YELLOW, touchColor, tolerance)) {
                            nextImage = R.drawable.g_selected;
                            assessmentValue = "1";
                        }
                    }

                    //blue area already selected
                    if (currentResource == R.drawable.b_selected) {
                        if (ct.closeMatch(Color.RED, touchColor, tolerance)) {
                            nextImage = R.drawable.rb_selected;
                            assessmentValue = "8";
                        } else if (ct.closeMatch(Color.BLUE, touchColor, tolerance)) {
                            nextImage = R.drawable.background_main;
                            assessmentValue = "0";
                        } else if (ct.closeMatch(Color.WHITE, touchColor, tolerance)) {
                            nextImage = R.drawable.wb_selected;
                            assessmentValue = "6";
                        } else if (ct.closeMatch(Color.YELLOW, touchColor, tolerance)) {
                            nextImage = R.drawable.g_selected;
                            assessmentValue = "1";
                        }
                    }

                    //white and red selected
                    if (currentResource == R.drawable.wr_selected) {
                        if (ct.closeMatch(Color.RED, touchColor, tolerance)) {
                            nextImage = R.drawable.w_selected;
                            assessmentValue = "2";
                        } else if (ct.closeMatch(Color.BLUE, touchColor, tolerance)) {
                            nextImage = R.drawable.wbr_selected;
                            assessmentValue = "7";
                        } else if (ct.closeMatch(Color.WHITE, touchColor, tolerance)) {
                            nextImage = R.drawable.r_selected;
                            assessmentValue = "3";
                        } else if (ct.closeMatch(Color.YELLOW, touchColor, tolerance)) {
                            nextImage = R.drawable.g_selected;
                            assessmentValue = "1";
                        }
                    }

                    //white and blue selected
                    if (currentResource == R.drawable.wb_selected) {
                        if (ct.closeMatch(Color.RED, touchColor, tolerance)) {
                            nextImage = R.drawable.wbr_selected;
                            assessmentValue = "8";
                        } else if (ct.closeMatch(Color.BLUE, touchColor, tolerance)) {
                            nextImage = R.drawable.w_selected;
                            assessmentValue = "2";
                        } else if (ct.closeMatch(Color.WHITE, touchColor, tolerance)) {
                            nextImage = R.drawable.b_selected;
                            assessmentValue = "4";
                        } else if (ct.closeMatch(Color.YELLOW, touchColor, tolerance)) {
                            nextImage = R.drawable.g_selected;
                            assessmentValue = "1";
                        }
                    }

                    //red and blue selected
                    if (currentResource == R.drawable.rb_selected) {
                        if (ct.closeMatch(Color.RED, touchColor, tolerance)) {
                            nextImage = R.drawable.b_selected;
                            assessmentValue = "4";
                        } else if (ct.closeMatch(Color.BLUE, touchColor, tolerance)) {
                            nextImage = R.drawable.r_selected;
                            assessmentValue = "3";
                        } else if (ct.closeMatch(Color.WHITE, touchColor, tolerance)) {
                            nextImage = R.drawable.wbr_selected;
                            assessmentValue = "7";
                        } else if (ct.closeMatch(Color.YELLOW, touchColor, tolerance)) {
                            nextImage = R.drawable.g_selected;
                            assessmentValue = "1";
                        }
                    }
                    //white, red and blue selected
                    if (currentResource == R.drawable.wbr_selected) {
                        if (ct.closeMatch(Color.RED, touchColor, tolerance)) {
                            nextImage = R.drawable.wb_selected;
                            assessmentValue = "6";
                        } else if (ct.closeMatch(Color.BLUE, touchColor, tolerance)) {
                            nextImage = R.drawable.wr_selected;
                            assessmentValue = "5";
                        } else if (ct.closeMatch(Color.WHITE, touchColor, tolerance)) {
                            nextImage = R.drawable.rb_selected;
                            assessmentValue = "8";
                        } else if (ct.closeMatch(Color.YELLOW, touchColor, tolerance)) {
                            nextImage = R.drawable.g_selected;
                            assessmentValue = "1";
                        }
                    }

                    //gold selected
                    if (currentResource == R.drawable.g_selected) {
                        if (ct.closeMatch(Color.RED, touchColor, tolerance)) {
                            nextImage = R.drawable.r_selected;
                            assessmentValue = "3";
                        } else if (ct.closeMatch(Color.BLUE, touchColor, tolerance)) {
                            nextImage = R.drawable.b_selected;
                            assessmentValue = "4";
                        } else if (ct.closeMatch(Color.WHITE, touchColor, tolerance)) {
                            nextImage = R.drawable.w_selected;
                            assessmentValue = "2";
                        } else if (ct.closeMatch(Color.YELLOW, touchColor, tolerance)) {
                            nextImage = R.drawable.background_main;
                            assessmentValue = "1";
                        }
                    }

                    // If the next image is the same as the last image, go back to the default.
                    // toast ("Current image: " + currentResource + " next: " + nextImage);

                    if (currentResource == nextImage) {
                        nextImage = R.drawable.background_main;
                    }

                    handledHere = true;
                    break;

                default:
                    handledHere = false;
            }// end switch

            //If handledHere is true and imageView is not null, set the background image to the
            //net image and set its tag
            if (handledHere) {
                if (nextImage > 0) {
                    imageView.setImageResource(nextImage);
                    imageView.setTag(nextImage);
                }
            }
        return handledHere;
    }

    /**
     * Get the color from the hotspot image at point x-y.
     */
    public int getHotspotColor(int hotspotId, int x, int y) {
        ImageView img = (ImageView) findViewById(hotspotId);
        if (img == null) {
            Log.d("AssessmentActivity", "Hot spot image not found");
            return 0;
        } else {
            img.setDrawingCacheEnabled(true);
            Bitmap hotspots = Bitmap.createBitmap(img.getDrawingCache());
            if (hotspots == null) {
                Log.d("AssessmentActivity", "Hot spot bitmap was not created");
                return 0;
            } else {
                img.setDrawingCacheEnabled(false);
                return hotspots.getPixel(x, y);
            }
        }
    }

    /**
     * The main button will take the user back to userMain. If the user is submitting their
     * assessment store assessmentValue under current date and time and change status of
     * needPostAssessment. Also replace users most recent assessmentValue with a new one
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submitBtn:
                if (Objects.equals(assessmentValue, "0")) {
                    Toast.makeText(AssessmentActivity.this, "Please Make Selection to Submit", Toast.LENGTH_LONG).show();
                    break;
                }

                //get current date
                DateFormat df = new SimpleDateFormat("HH:mm:ss a");
                String currentTime = df.format(Calendar.getInstance().getTime());
                String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                //changes needPostAssessment from false to true and stores new info
                try {
                    if (Objects.equals(needPostAssessment, "False")) {
                        FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Assessments").child(date).child(currentTime).child("time").setValue(currentTime);
                        FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Assessments").child(date).child(currentTime).child("type").setValue("pre");
                        FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Assessments").child(date).child(currentTime).child("result").setValue(assessmentValue);
                        FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("PostAssessment").setValue("True");

                        //changes needPostAssessment from true to false and stores new info
                    } else {
                        FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Assessments").child(date).child(currentTime).child("time").setValue(currentTime);
                        FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Assessments").child(date).child(currentTime).child("type").setValue("post");
                        FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Assessments").child(date).child(currentTime).child("result").setValue(assessmentValue);
                        FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("PostAssessment").setValue("False");
                    }

                    //store new recent assessmentValue
                    FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Assessments").child("recent").setValue(assessmentValue);
                }
                catch(DatabaseException UNKNOWN_ERROR){
                    Log.e("database",UNKNOWN_ERROR.getMessage());
                }

                //start Exercise Activity
                finish();
                startActivity(new Intent(this, ExerciseActivity.class));
                break;
            case R.id.backToUserMainBtn:
                //Go back to main
                try {
                    finish();
                    intent = new Intent(this, UserMain.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            case R.id.helpAssessment:
                try{
                    String help = getString(R.string.helpAssessment);
                    intent = new Intent(this, HelpPopup.class);
                    intent.putExtra("help_text", help);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
        }
    }
}