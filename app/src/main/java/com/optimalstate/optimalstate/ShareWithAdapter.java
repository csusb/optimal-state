/************************************************************************************
 * ShareWithAdapter
 *
 * This class adapter is used to generate the listView in ShareWith activity. It takes
 * a List of type Providers, and sets views for user name, viewability image and assigns
 * a checkbox.
 ***************************************************************************************/

package com.optimalstate.optimalstate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
import java.util.Objects;

public class ShareWithAdapter extends ArrayAdapter<Provider> {

    //variables
    private List<Provider> providerList;
    private Context context;

    //default constructor with parameters
    public ShareWithAdapter(List<Provider> providerList, Context context) {
        super(context, R.layout.share_with_list_adapter, providerList);
        this.providerList = providerList;
        this.context = context;
    }

    //nested class which holds view types
    private static class ProviderHolder {
        public TextView providerName;
        public ImageView visability;
        public CheckBox chkBox;
    }

    //sets views with information based on position in the List.
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        //create an instance of placehoder
        ProviderHolder holder = new ProviderHolder();

        //if null, initiate the layoutInflater
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.share_with_list_adapter, null);

            //assign the views to holder variables
            holder.providerName = (TextView) v.findViewById(R.id.providerName);
            holder.visability = (ImageView) v.findViewById(R.id.viewability);
            holder.chkBox = (CheckBox) v.findViewById(R.id.checkbox);

            //set a oncheckedlistener
            holder.chkBox.setOnCheckedChangeListener((ShareWith) context);
        } else {
            holder = (ProviderHolder) v.getTag();
        }

        //get one provider at a time and set views
        Provider p = providerList.get(position);
        holder.providerName.setText(p.getFirst() + " " + p.getLast());

        //Assign image based on their data for views
        if(Objects.equals(p.getView(),"true"))
            holder.visability.setBackgroundResource(R.drawable.ic_visable);
        else
            holder.visability.setBackgroundResource(R.drawable.ic_visable_off);

        //set box type based off it being selected or not
        holder.chkBox.setChecked(p.isSelected());
        holder.chkBox.setTag(p);

        return v;
    }
}