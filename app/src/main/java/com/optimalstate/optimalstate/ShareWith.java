/********************************************************************************************
 * ShareWith
 *
 * This class provides the user with options for sharing. It provides their unique userId which
 * they can copy to the clipboard, and share with providers. It also list those who currently
 * have access to viewing their history. The user may select the providers and disable their
 * ability to view them at any time. They may enable viewing again by following the same
 * process to disable
 ********************************************************************************************/

package com.optimalstate.optimalstate;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.Objects;

public class ShareWith extends Activity implements View.OnClickListener, android.widget.CompoundButton.OnCheckedChangeListener {

    //variables
    private Intent intent;
    private Button BackButton;
    private Button stopSahringButton;
    private TextView key;
    private ListView list;
    private Button copy;
    private ArrayList<Provider> providersList = new ArrayList<>();
    private ArrayList<Provider> nullShare = new ArrayList<>();
    private ArrayList<Provider> changes = new ArrayList<>();
    private String provider;
    private ImageView help;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_with);

        try{
            //getting context from intent about if user is a provider
            Bundle bundle = getIntent().getExtras();
            provider = bundle.getString("provider");


            //assigning views to variables
            BackButton = (Button) findViewById(R.id.backbutton);
            stopSahringButton = (Button) findViewById(R.id.submitChange);
            key = (TextView) findViewById(R.id.key);
            copy = (Button) findViewById(R.id.copyLink);
            list = (ListView) findViewById(R.id.friendsList);
            help = (ImageView) findViewById(R.id.help_shareWith);

            //get the user Id from the database and set to key
            key.setText(FirebaseUtil.getUserId());

        }
        catch(NullPointerException e) {
            Log.e("ShareWith", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ShareWith", e.getMessage());
        }

        //initialize null data to start
        nullData();

        //look to see if anyone is viewing user
        getProviders();

        //set onlick listeners
        help.setOnClickListener(this);
        copy.setOnClickListener(this);
        stopSahringButton.setOnClickListener(this);
        BackButton.setOnClickListener(this);
    }

    //If a box is selected, store that provider into a list for changes. If they are selected
    //again remove them from the list
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        try{
            int pos = list.getPositionForView(buttonView);
            if (pos != ListView.INVALID_POSITION) {
                Provider p = providersList.get(pos);
                p.setSelected(isChecked);
                if(!changes.contains(p))
                    changes.add(p);
                else
                    changes.remove(p);
            }
        }
        catch(NullPointerException e) {
            Log.e("ShareWith", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ShareWith", e.getMessage());
        }
    }

    //This function sets the listView to a null data set
    public void nullData() {
        try{
            Provider noShare = new Provider("No one viewing", "you", "", "False","");
            nullShare.add(noShare);
            ShareWithAdapter adapter = new ShareWithAdapter(nullShare, this);
            list.setAdapter(adapter);
        }
        catch(NullPointerException e) {
            Log.e("ShareWith", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ShareWith", e.getMessage());
        }
    }

    //This function updates the list with providers or new information
    public void updateList(ArrayList<Provider> newList) {
        try{
            list.setAdapter(null);
            ShareWithAdapter adapter = new ShareWithAdapter(newList, this);
            list.setAdapter(adapter);
        }
        catch(NullPointerException e) {
            Log.e("ShareWith", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ShareWith", e.getMessage());
        }
    }

    //performs actions if any onclick listerners are called
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //returns user to settings activity
            case R.id.backbutton:
                try{
                    finish();
                    intent = new Intent(this, SettingsActivity.class);
                    intent.putExtra("provider",provider);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            //Submit changes updates the data base for both the provider and user. getProviders is
            //then updated which calls updateList.
            case R.id.submitChange:
                for(Provider s: changes){
                    if(Objects.equals(s.getView(),"true")){
                       try{
                            FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Providers")
                                    .child(s.getId()).child("view").setValue("false");
                            FirebaseUtil.getRefdatabase().child(s.getId()).child("Clients").child(FirebaseUtil.getUserId())
                                    .child("view").setValue("false");
                            FirebaseUtil.getRefdatabase().child(s.getId()).child("Clients").child(FirebaseUtil.getUserId())
                                    .child("blocked").setValue("true");
                       }
                       catch(DatabaseException UNKNOWN_ERROR){
                           Log.e("database",UNKNOWN_ERROR.getMessage());
                       }
                    } else {
                       try {
                            FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Providers")
                                    .child(s.getId()).child("view").setValue("true");
                            FirebaseUtil.getRefdatabase().child(s.getId()).child("Clients").child(FirebaseUtil.getUserId())
                                    .child("view").setValue("true");
                            FirebaseUtil.getRefdatabase().child(s.getId()).child("Clients").child(FirebaseUtil.getUserId())
                                    .child("blocked").setValue("false");
                       }
                       catch(DatabaseException UNKNOWN_ERROR){
                           Log.e("database",UNKNOWN_ERROR.getMessage());
                       }
                    }
                }
                changes.clear();
                getProviders();
                break;
            //This function copys the contents of key and stores it to the devices clipboard
            case R.id.copyLink:
                try{
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("simple text", key.getText());
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(getApplicationContext(), "Text View Copied to Clipboard",
                            Toast.LENGTH_LONG).show();
                    break;
                }
                catch(NullPointerException e) {
                    Log.e("ShareWith", e.getMessage());
                }
                catch(Exception e) {
                    Log.e("ShareWith", e.getMessage());
                }
            //This function calls the help activity to display help information
            case R.id.help_shareWith:
                try{
                    String help = getString(R.string.helpShareWith);
                    intent = new Intent(this, HelpPopup.class);
                    intent.putExtra("help_text", help);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
        }
    }

    //This function searches the database for all providers who are viewing the user. They are
    //stored in providersList. If the list is empty nullData is called, else updateList is called
    public void getProviders() {
        FirebaseUtil.getRefdatabase().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    DataSnapshot getProviders = dataSnapshot.child(FirebaseUtil.getUserId()).child("Providers");
                    Iterable<DataSnapshot> provider = getProviders.getChildren();
                    providersList.clear();
                    for (DataSnapshot postSnapshot : provider) {
                        Provider person = postSnapshot.getValue(Provider.class);
                        if(Objects.equals(person.getBlocked(), "false"))
                            providersList.add(person);
                    }
                    if (providersList.isEmpty())
                        nullData();
                    else
                        updateList(providersList);
                }
                catch(DatabaseException UNKNOWN_ERROR){
                    Log.e("database",UNKNOWN_ERROR.getMessage());
                }
            }

            //log error if data failed to load
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(databaseError.getDetails(),"Error getting data");
            }
        });
    }
}