/********************************************************************************************
 * ProviderViewHistory
 *
 * THis class allows a provider to view the history of all their clients. A drop down menu
 * provides the provider with all their clients they currently have access to. The date
 * gives the provider a pop up interface to select a date of their choice. The listView will
 * then display all data for the given client for the given day. If no data is found, "no
 * data" will be displayed
 *******************************************************************************************/

package com.optimalstate.optimalstate;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

public class ProviderViewHistory extends Activity implements View.OnClickListener{


    //varialbes for views
    private ScrollView scroll;
    private Button backButton;
    private TextView dateSelecter;
    private ImageView help;

    //variables for dates
    private Intent intent;
    private DatePickerDialog.OnDateSetListener mOnDateSetListener;
    private String date;
    private String dateSearch;

    //variables for arraylist and spinners
    private ArrayList<Clients> clientList = new ArrayList<>();
    private ArrayList<Clients> nullClientList = new ArrayList<>();
    private Spinner clientSpinner;
    private String clientId;
    private ListView list;
    private ArrayList<Clients> nullClients = new ArrayList<>();
    private ArrayList<History> historyList = new ArrayList<>();
    private ArrayList<History> nullHistory = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_view_history);

        //assign views to variables
        try{
            backButton = (Button) findViewById(R.id.backbutton);
            list = (ListView) findViewById(R.id.providerHistoryView);
            scroll = (ScrollView) findViewById(R.id.scroll);
            dateSelecter = (TextView) findViewById(R.id.dateSelecter);
            help = (ImageView) findViewById(R.id.help_ProviderHistory);
            clientSpinner = (Spinner) findViewById(R.id.ClientSpinner);
        }
        catch(NullPointerException e) {
            Log.e("ProviderViewHistory", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ProviderViewHistory", e.getMessage());
        }

        //set onClickListeners
        help.setOnClickListener(this);
        dateSelecter.setOnClickListener(this);
        backButton.setOnClickListener(this);

        //call get date for todays date
        getDate();

        //this listener re-assigns the date after the user has selected a new date.
        //dateSearch is updated with the correct format that is needed to search the database.
        //After date is updated, getData is called to get data for that date
        mOnDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                try{
                    month = month + 1;
                    date = month + "/" + dayOfMonth + "/" + year;
                    dateSelecter.setText(date);
                    if(dayOfMonth < 10 && month < 10)
                        dateSearch = (year + "-" + "0" + month + "-" + "0"+ dayOfMonth).toString();
                    else if(dayOfMonth < 10)
                        dateSearch = (year + "-" + month + "-" + "0"+ dayOfMonth).toString();
                    else if(month < 10)
                        dateSearch = (year + "-" + "0" + month + "-" + dayOfMonth).toString();
                    else
                        dateSearch = (year + "-" + month + "-" + dayOfMonth).toString();
                    getData();
                }
                catch(NullPointerException e) {
                    Log.e("ProviderViewHistory", e.getMessage());
                }
                catch(Exception e) {
                    Log.e("ProviderViewHistory", e.getMessage());
                }
            }
        };

        //set clients to a default list
        nullClients();

        //set client data to a default list
        nullData();

        //get clients from database and update client list
        getClients();

        //This onItemSelectedListener, updates information when a new client is selected from
        //the clientSpinner. It pulls the Client object from the spinner and stores it into
        //clientId. Get data will then search data based off the clientId.
        clientSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int posistion, long id) {
                //store value that was selected
                try {
                    Clients client = (Clients) clientSpinner.getSelectedItem();
                    clientId = client.getId();
                    ((TextView) view).setGravity(Gravity.CENTER);
                    if (Objects.equals(clientId, "none"))
                        nullData();
                    else
                        getData();
                   }
                catch(NullPointerException e) {
                    Log.e("client spinner", e.getMessage());
                }
            }


            //When noting has been selected load data based off the first item. If the client id
            //is none, make call to set list to null. Else pass clientId  to update list
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                try{
                    Clients client = (Clients) clientSpinner.getSelectedItem();
                    clientId = client.getId().toString();
                    if(Objects.equals(clientId,"none"))
                        nullData();
                    else
                        getData();
                }
                catch(NullPointerException e) {
                    Log.e("ProviderViewHistory", e.getMessage());
                }
                catch(Exception e) {
                    Log.e("ProviderViewHistory", e.getMessage());
                }
                  }
        });

        //This onTouchListener will controll the scroll function. If touch, is withing listview,
        //listview will scroll. else entire screen will scroll.
        list.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                scroll.requestDisallowInterceptTouchEvent(true);

                int action = event.getActionMasked();

                switch (action) {
                    case MotionEvent.ACTION_UP:
                        scroll.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });
    }

    //This function obtains the clients that are listed under the provider. As data is pulled
    //from the database, it is stored as Client objects. The objects are then stored in a clientList
    //aa long as their view is true. The clientList is then passed to updateClient list. If there
    //are no clients, a nullClient function is called
    public void getClients() {
        clientList.clear();
        Clients selectClient = new Clients("none","Select","Clients","","");
        clientList.add(selectClient);
        FirebaseUtil.getRefdatabase().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    DataSnapshot client = dataSnapshot.child(FirebaseUtil.getUserId()).child("Clients");
                    Iterable<DataSnapshot> children = client.getChildren();
                    for (DataSnapshot postSnapshot : children) {
                        Clients addClient = postSnapshot.getValue(Clients.class);
                        if(Objects.equals(addClient.getView(),"true") && Objects.equals(addClient.getBlocked(),"false"))
                            clientList.add(addClient);
                    }
                    if (clientList.isEmpty())
                        nullClients();
                    else
                        updateClients(clientList);
                }
                catch(DatabaseException UNKNOWN_ERROR){
                    Log.e("database",UNKNOWN_ERROR.getMessage());
                }
            }

            //log error if failed to connect to database
            @Override
            public void onCancelled(DatabaseError databaseError) {
            Log.e(databaseError.getDetails(),"Failed to get data");
            }
        });
    }

        //This class sets the client list to a null list. A default Client object is created
        //which is meant to display that their are no clients. The spinner is then updated.
        public void nullClients(){
            try{
                Clients noHistory = new Clients("","No","Clients","","");
                nullClientList.add(noHistory);

                ArrayAdapter<Clients> adapter = new ArrayAdapter<Clients>(this,
                        android.R.layout.simple_spinner_dropdown_item, nullClientList){
                    //set the gravity of text to center
                    public View getDropDownView(int position, View convertView,ViewGroup parent) {

                        View v = super.getDropDownView(position, convertView,parent);
                        ((TextView) v).setGravity(Gravity.CENTER);
                        return v;
                    }
                };
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                clientSpinner.setAdapter(adapter);
            }
            catch(NullPointerException e) {
                Log.e("ProviderViewHistory", e.getMessage());
            }
            catch(Exception e) {
                Log.e("ProviderViewHistory", e.getMessage());
            }
        }

        //Similar to the above function, this function sets the spinner for clients. It takes
        //in a newList as a parameter and sets the spinner to that information.
        public void updateClients(ArrayList<Clients> newList){
            try{
            ArrayAdapter<Clients> adapter = new ArrayAdapter<Clients>(this,
                    android.R.layout.simple_spinner_dropdown_item, clientList){
                //set the gravity of text to center
                public View getDropDownView(int position, View convertView,ViewGroup parent) {

                    View v = super.getDropDownView(position, convertView,parent);
                    ((TextView) v).setGravity(Gravity.CENTER);
                    return v;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            clientSpinner.setAdapter(adapter);
            }
            catch(NullPointerException e) {
                Log.e("ProviderViewHistory", e.getMessage());
            }
            catch(Exception e) {
                Log.e("ProviderViewHistory", e.getMessage());
            }
        }


        //The function will update the list for a clients history. Takes a parameter of a ArrayList
        //of type history to update list
        public void updateList(ArrayList<History> newList){
            try{
                list.setAdapter(null);
                HistoryListAdapter adapter = new HistoryListAdapter(this,R.layout.history_adapter_layout, newList);
                list.setAdapter(adapter);
            }
            catch(NullPointerException e) {
                Log.e("ProviderViewHistory", e.getMessage());
            }
            catch(Exception e) {
                Log.e("ProviderViewHistory", e.getMessage());
            }
        }

        //This function retireves the history of the clients assessments. It is stored in objects of
        //type History. they are then stored in a Historylist. If HistoryList is empty, nullData is
        //called, else the list is updated with HistoryList.
        public void getData(){
            FirebaseUtil.getRefdatabase().addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try{
                        DataSnapshot assessments = dataSnapshot.child(clientId).child("Assessments").child(dateSearch);
                        Iterable<DataSnapshot> children = assessments.getChildren();
                        historyList.clear();
                        for (DataSnapshot postSnapshot: children){
                            History history = postSnapshot.getValue(History.class);
                            historyList.add(history);
                        }
                        if(historyList.isEmpty())
                            nullData();
                        else
                            updateList(historyList);
                    }
                    catch(DatabaseException UNKNOWN_ERROR){
                        Log.e("database",UNKNOWN_ERROR.getMessage());
                    }
                }

                //log error if data failed to be revtrieved
                @Override
                public void onCancelled(DatabaseError databaseError) {
                Log.e(databaseError.getDetails(),"failed to retrieve data");
                }
            });
        }

        //This function gets the date that a user has selected. The information is altered to format
        //how the date is searched in the database.
        public void getDate(){
            try{
                Calendar cal = Calendar.getInstance();
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int month = cal.get(Calendar.MONTH);
                int year = cal.get(Calendar.YEAR);
                month = month +1;
                if(day < 10 && month < 10)
                    dateSearch = (year + "-" + "0" + month + "-" + "0"+ day).toString();
                else if(day < 10)
                    dateSearch = (year + "-" + month + "-" + "0"+ day).toString();
                else if(month < 10)
                    dateSearch = (year + "-" + "0" + month + "-" + day).toString();
                else
                    dateSearch = (year + "-" + month + "-" + day).toString();
                date = month + "/" + day + "/" + year;
                dateSelecter.setText(date);
            }
            catch(NullPointerException e) {
                Log.e("ProviderViewHistory", e.getMessage());
            }
            catch(Exception e) {
                Log.e("ProviderViewHistory", e.getMessage());
            }
        }

        //This function will set the list data to null if their was no history available.
        public void nullData(){
            try{
                nullHistory.clear();
                History noHistory = new History("","No Data","");
                nullHistory.add(noHistory);
                HistoryListAdapter adapter = new HistoryListAdapter(this,R.layout.history_adapter_layout, nullHistory);
                list.setAdapter(adapter);
            }
            catch(NullPointerException e) {
                Log.e("ProviderViewHistory", e.getMessage());
            }
            catch(Exception e) {
                Log.e("ProviderViewHistory", e.getMessage());
            }
        }

        //If backButton or Help is selected, the appropriate activity will be started. If the date
        //is selected, a window is opened to display a date selector. The selected date is then
        //stored in a veriable to be displayed on the main activity.
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.backbutton:
                    try{
                        finish();
                        intent = new Intent(this,ProviderMain.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        break;
                    }
                    catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                case R.id.dateSelecter:
                    try{
                        Calendar cal = Calendar.getInstance();
                        int year = cal.get(Calendar.YEAR);
                        int month = cal.get(Calendar.MONTH);
                        int day = cal.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog dialog = new DatePickerDialog(
                                ProviderViewHistory.this,android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                                mOnDateSetListener,year,month,day);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.show();
                        break;
                    }
                    catch(NullPointerException e) {
                        Log.e("ProviderViewHistory", e.getMessage());
                    }
                    catch(Exception e) {
                        Log.e("ProviderViewHistory", e.getMessage());
                    }
                case R.id.help_ProviderHistory:
                    try{
                        String help = getString(R.string.helpProviderHistory);
                        intent = new Intent(this, HelpPopup.class);
                        intent.putExtra("help_text", help);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        break;
                    }
                    catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
            }
        }


}
