package com.optimalstate.optimalstate;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

public class ProviderRemoveClient extends AppCompatActivity implements View.OnClickListener, android.widget.CompoundButton.OnCheckedChangeListener {

    private Button removeButton;
    private Button backButton;

    private ListView list;
    private ScrollView scroll;
    private String name;

    private ArrayList<Clients> clientsList = new ArrayList<>();
    private ArrayList<Clients> nullClients = new ArrayList<>();
    private ArrayList<Clients> changes = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_remove_client);

        try{
            removeButton = (Button) findViewById(R.id.btnRemoveClient);
            backButton = (Button) findViewById(R.id.backbutton);
            list = (ListView) findViewById(R.id.lvClientList);
            scroll = (ScrollView) findViewById(R.id.scroll);
        }
        catch(NullPointerException e) {
            Log.e("ProviderRemoveClient", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ProviderRemoveClient", e.getMessage());
        }

        nullData();
        getClients();

        backButton.setOnClickListener(this);
        removeButton.setOnClickListener(this);
    }

    /*
     * The following function is executed whenever a checkbox is clicked. What it does is, add all the information
     * which is connected to the checkbox. In this case it get the associated client and their information
     * and pushes it onto an ArrayList called changes.
     *
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        try{
            int pos = list.getPositionForView(buttonView);
            if(pos != ListView.INVALID_POSITION){
                Clients c = clientsList.get(pos);
                c.setSelected(isChecked);
                if( !changes.contains(c) && !Objects.equals(c.getFirst(),"no data"))
                    changes.add(c);
                else
                    changes.remove(c);
            }
        }
        catch(NullPointerException e) {
            Log.e("ProviderRemoveClient", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ProviderRemoveClient", e.getMessage());
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnRemoveClient:
                if(!changes.isEmpty()) {
                    for (Clients s : changes) {
                        try {
                            FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Clients")
                                    .child(s.getId()).child("view").setValue("false");
                            FirebaseUtil.getRefdatabase().child(s.getId()).child("Providers").child(FirebaseUtil.getUserId())
                                    .child("view").setValue("false");
                            FirebaseUtil.getRefdatabase().child(s.getId()).child("Providers").child(FirebaseUtil.getUserId())
                                    .child("blocked").setValue("true");
                        } catch (DatabaseException e) {
                            Log.e("ProviderRemoveClient", e.getMessage());
                        }
                    }
                } else {
                    Toast.makeText(ProviderRemoveClient.this,"No Clients to Remove",Toast.LENGTH_SHORT).show();
                }
                changes.clear();
                getClients();
                break;
            case R.id.backbutton:
                try{
                    finish();
                    Intent intent = new Intent(this, ProviderMain.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
        }

    }

    public void updateList(ArrayList<Clients> newList){
        try{
            list.setAdapter(null);
            RemoveClientAdapter adapter = new RemoveClientAdapter(newList, this);
            list.setAdapter(adapter);
        }
        catch(NullPointerException e) {
            Log.e("ProviderRemoveClient", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ProviderRemoveClient", e.getMessage());
        }
    }


    public void getClients(){
        FirebaseUtil.getRefdatabase().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    DataSnapshot clients = dataSnapshot.child(FirebaseUtil.getUserId()).child("Clients");
                    Iterable<DataSnapshot> children = clients.getChildren();
                    clientsList.clear();

                    for(DataSnapshot postSnapShot : children){
                        Clients person = postSnapShot.getValue(Clients.class);
                        if(Objects.equals(person.getView(), "true"))
                            clientsList.add(person);
                    }

                    if(clientsList.isEmpty())
                        nullData();
                    else
                        updateList(clientsList);
                }
                catch(DatabaseException UNKNOWN_ERROR){
                    Log.e("database",UNKNOWN_ERROR.getMessage());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(databaseError.getDetails(),"Error getting data");
            }
        });
    }


    public void nullData(){
        try{
            nullClients.clear();
            Clients noClients = new Clients("","no data","","","");
            nullClients.add(noClients);
            RemoveClientAdapter adapter = new RemoveClientAdapter(nullClients, this);
            list.setAdapter(adapter);
        }
        catch(NullPointerException e) {
            Log.e("ProviderRemoveClient", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ProviderRemoveClient", e.getMessage());
        }
    }
}
