/*********************************************************************************************
 * Login_Activity
 *
 * This Activity is the starting activity for the Optimal State Application. This Activity
 * allows for exsisting users to login to their accounts. Once they login on their device
 * they will remain logged in until they access the log out function from the Settings
 * Activity. If a general user logins in, they will be redirected to the UserMainActivity,
 * providers will be redirected to the ProviderMainActivity. If a user does not have an
 * account they will select the register text which is displayed at the bottom of this
 * Activities interface. This will redirect them to the RegisterUserActivity.
 *
 *******************************************************************************************/

package com.optimalstate.optimalstate;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.ValueEventListener;
import java.util.Objects;

    /*
    This class implements OnClickListener which is defined at the end of this section.
    OnClickListener performs the actions that are exucuted when a button is pressed
    */

public class LoginActivity extends Activity implements View.OnClickListener {

    //Initializing LoginActivity.xml layouts
    private Button loginButton;
    private EditText loginEmailText;
    private EditText loginPasswordText;
    private Button registerBtn;

    //Initiializing a progress bar which can display messeges when action is taken
    private ProgressDialog progressDialog;
    private String provider;

    /*
    This function is executed as soon as activity loads
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //this prevents the keyboard from loading on start up
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //assign layouts to variables
        try{
            loginButton = (Button) findViewById(R.id.loginButton);
            loginEmailText = (EditText) findViewById(R.id.loginEmalText);
            loginPasswordText = (EditText) findViewById(R.id.loginPasswordText);
            registerBtn = (Button) findViewById(R.id.registerBtn);
        }
        catch(NullPointerException e) {
            Log.e("Exercise", e.getMessage());
        }
        catch(Exception e) {
            Log.e("Exercise", e.getMessage());
        }

        //create instance of progressDialog
        progressDialog = new ProgressDialog(this);

        //set OnClickListeners to layouts
        loginButton.setOnClickListener(this);
        registerBtn.setOnClickListener(this);
    }

        /*
        This function validations user input when logging in. It checks that fields are filled,
        valid emails, and valid passwords. If they are not, user will be notified of the error.
        Upon sussessful login, user will be taken to their MainActivity
         */
        private void userLogin(){
            try{
            //Create variables for email and password, converting them to string and triming them
            String email = loginEmailText.getText().toString().trim();
            String password = loginPasswordText.getText().toString().trim();

            //begin of validation. If no input was entered, display error message and highlight textfield
            if(email.isEmpty()){
                loginEmailText.setError("Email is required");
                loginEmailText.requestFocus();
                return;
            }

            //checks if input ressembles a valid email. If not, display error and highlight text field
            if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                loginEmailText.setError("Please enter a valid email");
                loginEmailText.requestFocus();
                return;
            }

            //checks if password was entered. If not displays error and highlights text field
            if(password.isEmpty()){
                loginPasswordText.setError("Password is required");
                loginPasswordText.requestFocus();
                return;
            }

            //If password is less than 6 char, display error and highlight textfield.
            //Can use more elaborate password requirments.
            if(password.length() < 6){
                loginPasswordText.setError("Minimum lenght of password should be 6");
                loginPasswordText.requestFocus();
                return;
            }

            //if validations passed, set message and display that logging in is being attempted
            progressDialog.setMessage("Logging In..");
            progressDialog.show();

            //This access FirebaseAuth to check if user and password match in database. Email and password from textfields
            //must be passed to this function
            FirebaseUtil.setFirebaseAuth();
            FirebaseUtil.getFirebaseAuth().signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    //If Email and password match, finish this task, and start UserMainActivity.
                    //create instances for firebase and user information
                    if(task.isSuccessful()){
                        try {
                            progressDialog.dismiss();
                            FirebaseUtil.setDatabase();
                            FirebaseUtil.setUser();
                            FirebaseUtil.setUserId();
                            FirebaseUtil.setRefdatabase();
                            getStatus();
                        }
                        catch(DatabaseException UNKNOWN_ERROR){
                                Log.e("database",UNKNOWN_ERROR.getMessage());
                        }
                        //If login failed, toast the exception message
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        }
                });
            }
            catch(NullPointerException e) {
                Log.e("Exercise", e.getMessage());
            }
            catch(Exception e) {
                Log.e("Exercise", e.getMessage());
            }
        }

    /*
    If a user has sussessfully logged into their account, this function will automatically finish this
    task and take user to their UserMainActivity or ProviderMainActivity
    */
    @Override
    protected void onStart() {
        super.onStart();
        if(FirebaseUtil.getFirebaseAuth() != null && FirebaseUtil.getUser() != null) {
            getStatus();
        }
       }


    /*
    If a OnClickListener is clicked, a switch case is used to figure which was called based on R.id.
    LoginButton calls userLogin function above, and AlreadyRegistered will take user to RegisterActivity.
     */
    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.loginButton:
                userLogin();
                break;
            case R.id.registerBtn:
                try{
                    finish();
                    Intent intent = new Intent(this, RegisterUser.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
        }
    }

    //If a user is currently signed in, check if they are a provider or not. If they are a
    //provider start the provider main, else start the user main.
    public void getStatus() {
            FirebaseUtil.getRefdatabase().addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    provider = dataSnapshot.child(FirebaseUtil.getUserId()).child("UserInfo").child("Provider").getValue(String.class);
                    if(Objects.equals(provider,"Yes")) {
                        try{
                            finish();
                            Intent intent = new Intent(LoginActivity.this, ProviderMain.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                        catch (ActivityNotFoundException e) {
                            e.printStackTrace();
                        }
                        catch(DatabaseException UNKNOWN_ERROR){
                            Log.e("database",UNKNOWN_ERROR.getMessage());
                        }

                    } else{
                        try{
                            finish();
                            Intent intent = new Intent(LoginActivity.this, UserMain.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                        catch (ActivityNotFoundException e) {
                            e.printStackTrace();
                        }
                        catch(DatabaseException UNKNOWN_ERROR){
                            Log.e("database",UNKNOWN_ERROR.getMessage());
                        }
                    }
                }
                //log error if database failed to retrieve data
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(databaseError.getDetails(),"Error loading data from database");
                }
            });
        }
}
