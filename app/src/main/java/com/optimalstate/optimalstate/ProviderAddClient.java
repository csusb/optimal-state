package com.optimalstate.optimalstate;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.app.ProgressDialog;
import android.nfc.Tag;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class ProviderAddClient extends Activity implements View.OnClickListener{

    private EditText etNewClientUserId;
    private TextView userInfo;
    private Button btnSearch;
    private Button btnAddNewClient;
    private Button btnBackProvider;
    private String first;
    private String last;
    private String id;
    private String providerLast;
    private String providerFirst;
    private String blocked;
    private String userId;
    private String user;
    private Boolean found;

    //Initiializing a progress bar which can display messeges when action is taken
    private ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_add_client);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        try{
            etNewClientUserId = (EditText) findViewById(R.id.etNewClientUserId);
            btnSearch = (Button) findViewById(R.id.btnSearch);
            btnAddNewClient = (Button) findViewById(R.id.btnAddClient);
            btnBackProvider = (Button) findViewById(R.id.btnBackProvider);
            userInfo = (TextView) findViewById(R.id.userinfo);
            progressDialog = new ProgressDialog(this);
        }
        catch(NullPointerException e) {
            Log.e("ProviderAddClient", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ProviderAddClient", e.getMessage());
        }

        btnSearch.setOnClickListener(this);
        btnAddNewClient.setOnClickListener(this);
        btnBackProvider.setOnClickListener(this);
    }


    private void SearchForClient(){
        try{
            userId = etNewClientUserId.getText().toString().trim();

            if  (userId.isEmpty()){
                etNewClientUserId.setError("UserId is required");
                etNewClientUserId.requestFocus();
                return;
            }
        }
        catch(NullPointerException e) {
            Log.e("ProviderAddClient", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ProviderAddClient", e.getMessage());
        }

        //this is just an option I looked into. Rather than searching by email, a general user
        //would provide their unique userID to the provider to search by. There needs to be
        //another button for this. One will be to search, the other will be to add. If its added
        //the user id will be stored under clients in the database.
        // database->UserId->clients
        // The providers userID needs to also be added to a section of the general users database.
        // database->UserId->watchers
        // There will need to be a boolean value that is set to true at first. If ether client
        // or provider choose to stop sharing or viewing, they can perform action to turn the
        // boolean to false
        FirebaseUtil.getRefdatabase().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    found = dataSnapshot.child(userId).exists();
                    if(found == true) {
                        user = dataSnapshot.child(userId).getKey();
                        first = dataSnapshot.child(user).child("UserInfo").child("first").getValue(String.class);
                        last = dataSnapshot.child(user).child("UserInfo").child("last").getValue(String.class);
                        id = userId;
                        userInfo.setText(first + " " + last);
                    }
                    else
                        userInfo.setText("No user found");

                }
                catch (DatabaseException e){
                    Log.e("ProviderAddClient",e.getMessage());
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(databaseError.getDetails(), "database error");
            }
        });
    }

    public void AddClientfn(){
        try{
            if  (Objects.equals(id,null)){
                etNewClientUserId.setError("Search for user first");
                etNewClientUserId.requestFocus();
                return;
            }
        }
        catch(NullPointerException e) {
            Log.e("Exercise", e.getMessage());
        }
        catch(Exception e) {
            Log.e("Exercise", e.getMessage());
        }

        FirebaseUtil.getRefdatabase().addListenerForSingleValueEvent (new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(found == true) {
                    try {
                        blocked = dataSnapshot.child(FirebaseUtil.getUserId()).child("Clients").child(id).child("blocked").getValue(String.class);
                        if (Objects.equals(blocked, "true"))
                            Toast.makeText(ProviderAddClient.this, "User has stopped you from viewing them", Toast.LENGTH_SHORT).show();
                        else {
                            providerFirst = dataSnapshot.child(FirebaseUtil.getUserId()).child("UserInfo").child("first").getValue(String.class);
                            providerLast = dataSnapshot.child(FirebaseUtil.getUserId()).child("UserInfo").child("last").getValue(String.class);
                            //add information to provider side
                            FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Clients").child(id).child("first").setValue(first);
                            FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Clients").child(id).child("last").setValue(last);
                            FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Clients").child(id).child("id").setValue(id);
                            FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Clients").child(id).child("view").setValue("true");
                            FirebaseUtil.getRefdatabase().child(FirebaseUtil.getUserId()).child("Clients").child(id).child("blocked").setValue("false");

                            //add information to client side
                            FirebaseUtil.getRefdatabase().child(id).child("Providers").child(FirebaseUtil.getUserId()).child("first").setValue(providerFirst);
                            FirebaseUtil.getRefdatabase().child(id).child("Providers").child(FirebaseUtil.getUserId()).child("last").setValue(providerLast);
                            FirebaseUtil.getRefdatabase().child(id).child("Providers").child(FirebaseUtil.getUserId()).child("id").setValue(FirebaseUtil.getUserId());
                            FirebaseUtil.getRefdatabase().child(id).child("Providers").child(FirebaseUtil.getUserId()).child("view").setValue("true");
                            FirebaseUtil.getRefdatabase().child(id).child("Providers").child(FirebaseUtil.getUserId()).child("blocked").setValue("false");
                            Toast.makeText(ProviderAddClient.this, "Successfully Added", Toast.LENGTH_SHORT).show();
                        }
                    } catch (DatabaseException UNKNOWN_ERROR) {
                        Log.e("database", UNKNOWN_ERROR.getMessage());
                    }
                } else {
                    Toast.makeText(ProviderAddClient.this, "Get correct userId", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(databaseError.getDetails(),"Error occurred while retrieving data");
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSearch:
                SearchForClient();
                break;
            case R.id.btnAddClient:
                AddClientfn();
                break;
            case R.id.btnBackProvider:
                try{
                    finish();
                    Intent intent = new Intent(this, ProviderMain.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
        }
    }

}
