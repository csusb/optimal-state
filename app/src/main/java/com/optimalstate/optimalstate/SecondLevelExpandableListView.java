package com.optimalstate.optimalstate;

import android.content.Context;
import android.widget.ExpandableListView;

/**
 * This class is needed when using a multi leveled listView. it provides a measure
 * for the lowest level of the list view
 */
public class SecondLevelExpandableListView extends ExpandableListView{
    public SecondLevelExpandableListView(Context context){
        super(context);
    }

    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(999999,MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec,heightMeasureSpec);
    }
}
