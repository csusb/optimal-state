/**********************************************************************************************
 * ExerciseActivity
 *
 * This activity provides users information that can help improve or maintain their current
 * mental state. The interface provides a three tier expandable list which provides information
 * based on the users recent Assessment value. Different assessment values will provide
 * different results. All Strings are located from the ExerciseUtil class.
 **********************************************************************************************/

package com.optimalstate.optimalstate;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

public class ExerciseActivity extends Activity implements View.OnClickListener {

    //declaring variables
    private Intent intent;
    private ExpandableListView expandableListView;
    private Button backButton;
    private Button assessBtn;
    private DisplayMetrics metrics;
    private int width;
    private String recent;
    private String needPostAssessment;
    private int recentInt;
    private ImageView help;
    private ExerciseUtil mExerciseUtil = new ExerciseUtil();

    //declaring hashMaps for each level of expandableListView
    LinkedHashMap<String, String[]> thirdLevel1 = new LinkedHashMap<>();
    LinkedHashMap<String, String[]> thirdLevel2 = new LinkedHashMap<>();
    LinkedHashMap<String, String[]> thirdLevel3 = new LinkedHashMap<>();
    LinkedHashMap<String, String[]> thirdLevel4 = new LinkedHashMap<>();
    LinkedHashMap<String, String[]> thirdLevel5 = new LinkedHashMap<>();

    //declaring a List of String[] for the second level of expandableListView
    List<String[]> secondLevel = new ArrayList<>();

    //declaring variable that will contain all data for each level
    List<LinkedHashMap<String,String[]>> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);
        assessBtn = findViewById(R.id.assessBtn);

        //retrieve data from database. Getting users recent assessment value and assessment statue
        FirebaseUtil.getRefdatabase().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //get recent value and convert it to an int
                try{
                    recent = dataSnapshot.child(FirebaseUtil.getUserId()).child("Assessments").child("recent").getValue(java.lang.String.class);
                    recentInt = Integer.parseInt(recent);

                    //check if recent is 5,6, or 7 and change it to 2. only 1-4 is allowed
                    if(Objects.equals(recentInt,5) || Objects.equals(recentInt,6) || Objects.equals(recentInt,7))
                        recentInt = 2;

                    //check if recent is 8 and change it to 3. only 1-4 is allowed
                    if(Objects.equals(recentInt,8))
                        recentInt = 3;

                    //get assessment statues and disable button if they dont need a post assessment
                    needPostAssessment = dataSnapshot.child(FirebaseUtil.getUserId()).child("PostAssessment").getValue(String.class);
                    if(Objects.equals(needPostAssessment, "False"))
                        assessBtn.setVisibility(View.GONE);

                    //if data has changed, re-initialize the adapter
                    setUpAdapter();
                }
                catch(DatabaseException UNKNOWN_ERROR){
                    Log.e("database",UNKNOWN_ERROR.getMessage());
                }
            }

            //log error if loading data fails
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(databaseError.getDetails(),"Error retieving data");
            }
        });

        //set up adapter
        setUpAdapter();

        //get the information of the screen size that current device is using
        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        width = metrics.widthPixels;

        //set id to button
        try{
            backButton = (Button) findViewById(R.id.backbutton);
            help = (ImageView) findViewById(R.id.helpExercises);
        }
        catch(NullPointerException e) {
            Log.e("Exercise", e.getMessage());
        }
        catch(Exception e) {
            Log.e("Exercise", e.getMessage());
        }

        //set onClickListeners
        help.setOnClickListener(this);
        assessBtn.setOnClickListener(this);
        backButton.setOnClickListener(this);

        //Set on groupClickListener for the expandableListView. This will readjust the current layout
        //being displayed
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                setListViewHeight(parent, groupPosition);
                return false;
            }
        });
    }

    //This function adjusts the height of the expandableListView based on the number of parent fields
    //and the number of child fields that are present or expanded. This allows for the listView to
    //be nested inside of a scrollView in the xml file
    private void setListViewHeight(ExpandableListView listView, int group) {
        try{
            ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();

            //create and set height and width to starting values
            int totalHeight = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),View.MeasureSpec.EXACTLY);

            //loop through the list and get a group count
            for (int i = 0; i < listAdapter.getGroupCount(); i++) {
                View groupItem = listAdapter.getGroupView(i, false, null, listView);
                groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                //add measure of each item to totalheight
                totalHeight += groupItem.getMeasuredHeight();

                //This part is only done if the list has been expaned out
                if (((listView.isGroupExpanded(i)) && (i != group))
                        || ((!listView.isGroupExpanded(i)) && (i == group))) {

                    //loop through and count number of children that are expanded
                    for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                        View listItem = listAdapter.getChildView(i, j, false, null,
                                listView);
                        listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                        //add measure of each item to totalHeight
                        totalHeight += listItem.getMeasuredHeight();
                    }
                }
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();

            //get the final height that will be changed too
            int height = totalHeight
                    + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));

            //if height is less than 10, default to 200
            if (height < 10)
                height = 200;

            //set new layout height
            params.height = height;
            listView.setLayoutParams(params);
            listView.requestLayout();
            }
        catch(NullPointerException e) {
            Log.e("Exercise", e.getMessage());
        }
        catch(Exception e) {
            Log.e("Exercise", e.getMessage());
        }
    }

    //This function performs the action associated with the given button. Each button
    //sets the expandableListView to null and starts a new activity
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backbutton:
                try{
                    expandableListView.setAdapter((BaseExpandableListAdapter)null);
                    finish();
                    intent = new Intent(this, UserMain.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            case R.id.assessBtn:
                try{
                    expandableListView.setAdapter((BaseExpandableListAdapter)null);
                    finish();
                    intent = new Intent(this, AssessmentActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            case R.id.helpExercises:
                try{
                    String help = getString(R.string.helpExercise);
                    intent = new Intent(this, HelpPopup.class);
                    intent.putExtra("help_text", help);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }

        }
    }

    //Supporting function to get the scale factor based on the device being used
    public int GetDipsFromPixel(float pixels)
    {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f );
    }

    //This function sets up the adapter for the expandableListView. It retreives the strings
    //from the ExerciseUtil class and stores them in the correct level. In case of data change
    //each initialation will clear the layer before adding to it.
    private void setUpAdapter() {

        //set all strings in ExerciseUtil based on recent assessment
        try{
           if (recentInt == 0)
               mExerciseUtil.setAllStrings(0);
           else
               mExerciseUtil.setAllStrings(recentInt);

            //load strings for parent and child headers
            String[] parent = mExerciseUtil.getParent();
            String[] child1 = mExerciseUtil.getChildExercise();
            String[] child2 = mExerciseUtil.getChildYoga();
            String[] child3 = mExerciseUtil.getChildBreathing();
            String[] child4 = mExerciseUtil.getChildMeditation();
            String[] child5 = mExerciseUtil.getChildDiet();

            //load all child headers into secondLevel
            secondLevel.clear();
            secondLevel.add(child1);
            secondLevel.add(child2);
            secondLevel.add(child3);
            secondLevel.add(child4);
            secondLevel.add(child5);
            thirdLevel1.clear();

            //if a user has not taken an assessment, there will be no data to load, so the for
            //loop will be skipped for adding subchildren. If they have taken assessment, five
            //children will be the max information that is loaded
            int size;
            if (recentInt == 0)
                size = 0;
            else
                size = 5;

            //loop through and assign each child their corasponding subchild
            for (int i = 0; i < size; i++) {
                mExerciseUtil.setSubChildExercise(recentInt, i);
                String[] subchild1 = mExerciseUtil.getSubChildExercise();

                mExerciseUtil.setSubChildYoga(recentInt, i);
                String[] subchild2 = mExerciseUtil.getSubChildYoga();

                mExerciseUtil.setSubChildBreathing(recentInt, i);
                String[] subchild3 = mExerciseUtil.getSubChildBreathing();

                mExerciseUtil.setSubChildMeditation(recentInt, i);
                String[] subchild4 = mExerciseUtil.getSubChildMeditation();

                mExerciseUtil.setSubChildDiet(recentInt, i);
                String[] subchild5 = mExerciseUtil.getSubChildDiet();

                //skip adding to third level if the string is empty
                if (subchild1[0] != "")
                    thirdLevel1.put(child1[i], subchild1);

                if (subchild2[0] != "")
                    thirdLevel2.put(child2[i], subchild2);

                if (subchild3[0] != "")
                    thirdLevel3.put(child3[i], subchild3);

                if (subchild4[0] != "")
                    thirdLevel4.put(child4[i], subchild4);

                if (subchild5[0] != "")
                    thirdLevel5.put(child5[i], subchild5);
            }

            //add the contents of the second and third level headers into the data list
            data.clear();
            data.add(thirdLevel1);
            data.add(thirdLevel2);
            data.add(thirdLevel3);
            data.add(thirdLevel4);
            data.add(thirdLevel5);

            //initialize expandableListView and attach to the adapter
            expandableListView = (ExpandableListView) findViewById(R.id.expandable);

            //bounds are set for indicator arrows that will be displayed
            expandableListView.setIndicatorBounds(width - GetDipsFromPixel(70), width - GetDipsFromPixel(30));
            MyAdapter myAdapter = new MyAdapter(this, parent, secondLevel, data);
            expandableListView.setAdapter(myAdapter);

            //this function will collapse other list if another one is clicked
            expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                int previousGroup = -1;

                @Override
                public void onGroupExpand(int groupPosition) {
                    if (groupPosition != previousGroup)
                        expandableListView.collapseGroup(previousGroup);
                    previousGroup = groupPosition;
                }
            });
        }
        catch(NullPointerException e) {
            Log.e("Exercise", e.getMessage());
        }
        catch(Exception e) {
            Log.e("Exercise", e.getMessage());
        }
    }
}