/**********************************************************************************************
 * UserMainActivity
 *
 * This activity is the main interface for general users. The screen will greet the user with
 * their first and last name. The user will then have four options from which they can choose
 * from. The firebase database will retrieve the users assessment status and display for the
 * user to ether take an assessment or post assessment. Other options will include exercises,
 * history and settings. The user may also choose to log out of their account.
 **********************************************************************************************/

package com.optimalstate.optimalstate;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.ValueEventListener;
import java.util.Objects;

public class UserMain extends Activity implements View.OnClickListener{

    //declaring variables
    Intent intent;
    private TextView textWelcomeName;
    private Button settingsButton;
    private Button exerciseButton;
    private Button logOutButton;
    private Button assessButton;
    private Button historyButton;
    private String needPostAssessment;
    private String provider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);

        try{
            //Assigning buttons to id's
            assessButton = findViewById(R.id.assessButton);
            exerciseButton = findViewById(R.id.exerciseButton);
            settingsButton = findViewById(R.id.settingsButton);
            textWelcomeName = findViewById(R.id.textWelcomeName);
            logOutButton = findViewById(R.id.logOutButton);
            historyButton = findViewById(R.id.viewHistoryButton);
        }
        catch(NullPointerException e) {
            Log.e("UserMain", e.getMessage());
        }
        catch(Exception e) {
            Log.e("UserMain", e.getMessage());
        }

        //Setting onclick listeners for buttons
        settingsButton.setOnClickListener(this);
        logOutButton.setOnClickListener(this);
        assessButton.setOnClickListener(this);
        exerciseButton.setOnClickListener(this);
        historyButton.setOnClickListener(this);

        //Retrieve users first and last name along with their assessment status.
        FirebaseUtil.getRefdatabase().addListenerForSingleValueEvent (new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    String first = dataSnapshot.child(FirebaseUtil.getUserId()).child("UserInfo").child("first").getValue(String.class);
                    String last = dataSnapshot.child(FirebaseUtil.getUserId()).child("UserInfo").child("last").getValue(String.class);
                    needPostAssessment = dataSnapshot.child(FirebaseUtil.getUserId()).child("PostAssessment").getValue(String.class);
                    provider = dataSnapshot.child(FirebaseUtil.getUserId()).child("UserInfo").child("Provider").getValue(String.class);
                    //set users first and last name to textField
                    textWelcomeName.setText(first + " " + last);

                    //change text of assessment button if they need to take a post assessment
                    if(Objects.equals(needPostAssessment, "True")){
                        assessButton.setText("Take Post Assessment");
                    } else {
                        assessButton.setText("Take Assessment");
                    }
                }
                catch(DatabaseException UNKNOWN_ERROR){
                    Log.e("database",UNKNOWN_ERROR.getMessage());
                }
            }

            //record error if retrieving data failed
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(databaseError.getDetails(),"Error occurred while retrieving data");
            }
        });
    }

    //this function will perform an action based on the button thats been touched. Each starts
    //an activity that is associated with it. LogOutButton will call call the Firebase class
    //to terminate current instances
    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.assessButton:
                try{
                    finish();
                    intent = new Intent(this,AssessmentActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            case R.id.exerciseButton:
                try{
                    finish();
                    intent = new Intent(this, ExerciseActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            case R.id.viewHistoryButton:
                try{
                    finish();
                    intent = new Intent(this,ViewHistory.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            case R.id.settingsButton:
                try{
                    finish();
                    intent = new Intent(this,SettingsActivity.class);
                    intent.putExtra("provider",provider);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            case R.id.logOutButton:
                try{
                    FirebaseUtil.signout();
                    finish();
                    intent = new Intent(this,LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
        }
    }
}
