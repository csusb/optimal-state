/******************************************************************************************
 * SecondLevelAdapter
 *
 * This class performs the same operation as MyAdapter except it will not have any more
 * subchildren beyond this level. This class requires an additional class which will
 * provided the needed measure for each subchild
 ******************************************************************************************/

package com.optimalstate.optimalstate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

public class SecondLevelAdapter extends BaseExpandableListAdapter {

    //declaring variables
    private Context context;
    List<String[]> data;
    String[] headers;
    ImageView ivGroupIndicator;

    //constructor
    public SecondLevelAdapter(Context context,String[] headers, List<String[]> data){
        this.context = context;
        this.data = data;
        this.headers = headers;
    }

    //returns second level headers
    @Override
    public Object getGroup(int groupPosition) {
        return headers[groupPosition];
    }

    //returns second level header size
    @Override
    public int getGroupCount(){
        return headers.length;
    }

    //returns id's of second level headers
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    //This function creates an instance of inflater so that each second level heading can be
    //expanded
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){

        //create instance of inflater
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.child_layout,null);

        //assign strings to second level
        TextView text = (TextView) convertView.findViewById(R.id.child_heading);
        String groupText = getGroup(groupPosition).toString();
        text.setText(groupText);

        //assign image to second level headings
        ivGroupIndicator = (ImageView) convertView.findViewById(R.id.ivGroupIndicator);
        ivGroupIndicator.setSelected(isExpanded);
        return convertView;
    }

    //returns subchild position
    @Override
    public Object getChild(int groupPosition, int childPosistion){
        String[] childData;
        childData = data.get(groupPosition);
        return childData[childPosistion];
    }

    //returns subchild id
    @Override
    public long getChildId(int groupPosistion, int childPosition){
        return childPosition;
    }

    //This class creates an instance of inflater. While this level will not be expandable, it
    //allows for future additions to more levels
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent){

        //create instance of inflater
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.subchild_layout,null);

        //assign strins to textViews of the third level
        TextView textView = (TextView) convertView.findViewById(R.id.subchild_heading);
        String[] childArray = data.get(groupPosition);
        String text = childArray[childPosition];
        textView.setText(text);
        return convertView;
    }

    //returns the size of subchildren. if there are no children returns 0
    @Override
    public int getChildrenCount(int groupPosition){
        if(data.size() == 0)
            return 0;

        String[] children = data.get(groupPosition);
        return children.length;
    }

    //checks if ids are good
    @Override
    public boolean hasStableIds(){
        return true;
    }

    //allows subchild to be selectable. Will not expand if they don't have children
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition){
        return true;
    }
}
