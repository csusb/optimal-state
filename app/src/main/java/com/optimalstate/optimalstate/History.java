/**********************************************************************************
 * History
 *
 * This class saves a users assessment history. It stores the times, types and results
 * from all assessments for the given day. This class provides getters and setters
 * to store and pass information
 *********************************************************************************/

package com.optimalstate.optimalstate;

public class History {
    //variables
    public String time;
    public String type;
    public String result;

    //default constructor, no parameters
    public History()
    {}

    //default constructor, with parameters
    public History(String result,String time, String type) {
        this.time = time;
        this.type = type;
        this.result = result;
    }

    //returns type of assessment
    public String getType() {
        return type;
    }

    //sets the type of assessment
    public void setType(String type) {
        this.type = type;
    }

    //returns result of assessment
    public String getResult() {
        return result;
    }

    //set the type of result
    public void setResult(String result) {
        this.result = result;
    }

    //return time of assessment
    public String getTime() {
        return time;
    }

    //set time of assessment
    public void setTime(String time) {
        this.time = time;
    }
}
