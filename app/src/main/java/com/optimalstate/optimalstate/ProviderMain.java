/**********************************************************************************************
 * ProviderMainActivity
 *
 * This activity is the main interface for providers. The screen will greet the provider with
 * their first and last name. The provider will then have four options from which they can choose
 * from. These include add client, remove client, view client history, and settings. The provider
 * may also choose to log out of their account.
 **********************************************************************************************/

package com.optimalstate.optimalstate;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.ValueEventListener;

public class ProviderMain extends Activity implements View.OnClickListener{

    private Intent intent;
    private TextView textWelcomeName;
    private Button providerSettingsButton;
    private Button removeClientButton;
    private Button addClientButton;
    private Button viewClientHistoryButton;
    private Button providerLogOutButton;
    private String provider;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_main);

        //Assigning buttons to id's
        try{
            removeClientButton = findViewById(R.id.removeClientButton);
            addClientButton = findViewById(R.id.addClientButton);
            providerSettingsButton = findViewById(R.id.providerSettingsButton);
            textWelcomeName = findViewById(R.id.textWelcomeName);
            providerLogOutButton = findViewById(R.id.providerLogOutButton);
            viewClientHistoryButton = findViewById(R.id.viewClientHistoryButton);
        }
        catch(NullPointerException e) {
            Log.e("ProviderMain", e.getMessage());
        }
        catch(Exception e) {
            Log.e("ProviderMain", e.getMessage());
        }

        //Setting onclick listeners for buttons
        providerSettingsButton.setOnClickListener(this);
        providerLogOutButton.setOnClickListener(this);
        removeClientButton.setOnClickListener(this);
        addClientButton.setOnClickListener(this);
        viewClientHistoryButton.setOnClickListener(this);

        //Retrieve users first and last name.
        FirebaseUtil.getRefdatabase().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    String first = dataSnapshot.child(FirebaseUtil.getUserId()).child("UserInfo").child("first").getValue(String.class);
                    String last = dataSnapshot.child(FirebaseUtil.getUserId()).child("UserInfo").child("last").getValue(String.class);
                    provider = dataSnapshot.child(FirebaseUtil.getUserId()).child("UserInfo").child("Provider").getValue(String.class);

                    //set users first and last name to textField
                    textWelcomeName.setText(first + " " + last);
                }
                catch(DatabaseException UNKNOWN_ERROR){
                    Log.e("database",UNKNOWN_ERROR.getMessage());
                }
            }

            //record error if retrieving data failed
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(databaseError.getDetails(),"Error occurred while retrieving data");
            }
        });
    }

    //this function will perform an action based on the button thats been touched. Each starts
    //an activity that is associated with it. providerLogOutButton will call call the Firebase class
    //to terminate current instances
    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.addClientButton:
                try{
                    finish();
                    intent = new Intent(this,ProviderAddClient.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            case R.id.removeClientButton:
                try{
                    finish();
                    intent = new Intent(this, ProviderRemoveClient.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            case R.id.providerSettingsButton:
                try{
                    finish();
                    intent = new Intent(this,SettingsActivity.class);
                    intent.putExtra("provider",provider);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            case R.id.providerLogOutButton:
                try{
                    FirebaseUtil.signout();
                    finish();
                    intent = new Intent(this,LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            case R.id.viewClientHistoryButton:
                try{
                    finish();
                    intent = new Intent(this,ProviderViewHistory.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }
                catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
        }
    }
}
