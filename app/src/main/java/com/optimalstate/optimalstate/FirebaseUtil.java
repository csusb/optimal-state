/*****************************************************************************************
 * FirebaseUtil
 *
 * This class provides a single location for firebase instances to be stored. This prevents
 * multiple instance from being created which could cause possible error during heavy
 * traffic. Each variable is set when the user first logs in or registers an account.
 * Getters are used when data is to be stored or retrieved from database. The log out
 * function ends each instance by declaring the variables null
 *****************************************************************************************/

package com.optimalstate.optimalstate;

import android.app.Application;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseUtil extends Application {

    //Each variable will remain constant
    private static FirebaseAuth firebaseAuth = null;
    private static FirebaseUser user = null;
    private static FirebaseDatabase database = null;
    private static DatabaseReference refdatabase = null;
    private static String userId = null;

    //returns instance of FirebaseAuth
    public static FirebaseAuth getFirebaseAuth() {
        return firebaseAuth;
    }

    //sets a single instance of FirebaseAuth
    public static void setFirebaseAuth() {
        if(FirebaseUtil.firebaseAuth == null) {
            FirebaseUtil.firebaseAuth = FirebaseAuth.getInstance();
        }
    }

    //returns the current user
    public static FirebaseUser getUser() {
        return user;
    }

    //sets the current user as long as firebaseAuth is not null
    public static void setUser() {
        if(FirebaseUtil.user == null && FirebaseUtil.firebaseAuth != null){
            FirebaseUtil.user = FirebaseUtil.firebaseAuth.getCurrentUser();
        }
    }

    //returns instance of the database
    public static FirebaseDatabase getDatabase() {
        return database;
    }

    //sets a signel instance of the database. also sets a single instance of persistence
    //which will store data if internet is lost
    public static void setDatabase() {
        if(FirebaseUtil.database == null){
            FirebaseUtil.database = FirebaseDatabase.getInstance();
            FirebaseUtil.database.setPersistenceEnabled(true);
        }
    }

    //returns the current users userId
    public static String getUserId() {
        return userId;
    }

    //set the userId to the current user. As long as a current user is not null
    public static void setUserId() {
        if(FirebaseUtil.userId == null && FirebaseUtil.user != null){
            FirebaseUtil.userId = FirebaseUtil.user.getUid();
        }
    }

    //log out of firebaseAuth and set all instance to null
    public static void signout() {
        FirebaseUtil.firebaseAuth.signOut();
        FirebaseUtil.firebaseAuth = null;
        FirebaseUtil.refdatabase = null;
        FirebaseUtil.user = null;
        FirebaseUtil.userId = null;
    }

    //returns a referance to the database
    public static DatabaseReference getRefdatabase() {
        return refdatabase;
    }

    //sets a reference to the current database
    public static void setRefdatabase() {
        FirebaseUtil.refdatabase = database.getInstance().getReference();
    }
}