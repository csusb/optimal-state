/***************************************************************************************
 * HistoryListAdapter
 *
 * This class is an adapter for the listView found in ViewHistory activity. It takes
 * an input of objects of type History to populate the list. Based on the Objects
 * result variable, the images are set to different colors. Time and type are also
 * set to the same line of the list
 ***************************************************************************************/

package com.optimalstate.optimalstate;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Objects;

public class HistoryListAdapter extends ArrayAdapter<History> {

    //variables
    private static final String TAG = "HistoryListAdapter";
    private Context mContext;
    int mResoucse;

    //default constructor
    public HistoryListAdapter(Context context, int resource, ArrayList<History> object){
        super(context,resource,object);
        mContext = context;
        mResoucse = resource;
    }

    //this class sets the view for each line of the ListView. It sets the Time, Type and
    //Results of assessment
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        //get variables from ArrayList of type History
        String time = getItem(position).getTime();
        String status = getItem(position).getType();
        String results = getItem(position).getResult();

        //Set layout inflater
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResoucse,parent,false);

        //set views to R.id
        TextView textTime = (TextView) convertView.findViewById(R.id.time);
        TextView textStatus = (TextView) convertView.findViewById(R.id.status);
        ImageView image1 = (ImageView) convertView.findViewById(R.id.historyImage1);
        ImageView image2 = (ImageView) convertView.findViewById(R.id.historyImage2);
        ImageView image3 = (ImageView) convertView.findViewById(R.id.historyImage3);

        //set text for time and type to correct views
        textTime.setText(time);
        textStatus.setText(status);

        //Set the image background color based on the result. If no results backgrounds
        //will remain clear. Up to 3 different imageViews may be set
        if(Objects.equals(results,"1")){
            image1.setBackgroundResource(R.drawable.gold_history);
        }

        if(Objects.equals(results,"2")){
            image1.setBackgroundResource(R.drawable.white_history);
        }

        if(Objects.equals(results,"3")){
            image1.setBackgroundResource(R.drawable.red_history);
        }

        if(Objects.equals(results,"4")){
            image1.setBackgroundResource(R.drawable.blue_history);
        }

        if(Objects.equals(results,"5")){
            image1.setBackgroundResource(R.drawable.white_history);
            image2.setBackgroundResource(R.drawable.red_history);
        }
        if(Objects.equals(results,"6")){
            image1.setBackgroundResource(R.drawable.white_history);
            image2.setBackgroundResource(R.drawable.blue_history);
        }
        if(Objects.equals(results,"7")){
            image1.setBackgroundResource(R.drawable.white_history);
            image2.setBackgroundResource(R.drawable.red_history);
            image3.setBackgroundResource(R.drawable.blue_history);
        }
        if(Objects.equals(results,"8")){
            image1.setBackgroundResource(R.drawable.red_history);
            image2.setBackgroundResource(R.drawable.blue_history);
        }

        //every other position assign a different background for listview
        if(position % 2 == 1)
            convertView.setBackgroundColor(Color.parseColor("#fee4be"));
        else
            convertView.setBackgroundColor(Color.parseColor("#FFFFFF"));

        //return the updated list view
        return convertView;
    }
}