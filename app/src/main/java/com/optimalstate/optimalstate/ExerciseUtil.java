/*******************************************************************************************
 * ExerciseUtil
 *
 * This class is a supporting class for ExerciseActivity. It provides the needed strings
 * for the expandableListView. Based on the value passed for the users recent assessment,
 * the corrasponding strings are set and returned. As more information is provided,
 * additional functions will be created to support a rotating or random selection of the
 * strings for each catagory
 *******************************************************************************************/

package com.optimalstate.optimalstate;

public class ExerciseUtil {

    //initialize variables
    private String[] parent;
    private String[] childExercise;
    private String[] childYoga;
    private String[] childBreathing;
    private String[] childMeditation;
    private String[] childDiet;
    private String[] subChildExercise;
    private String[] subChildYoga;
    private String[] subChildBreathing;
    private String[] subChildMeditation;
    private String[] subChildDiet;

    //set all strings based off the recent variable
    public void setAllStrings(int recent){
        setParent();
        setChildExercise(recent);
        setChildYoga(recent);
        setChildMeditation(recent);
        setChildBreathing(recent);
        setChildDiet(recent);
    }

    //returns the paret strings
    public String[] getParent() {
        return parent;
    }

    //set parent strings. They will always return these 5
    public void setParent() {
        parent = new String[]{"Daily Exercise","Yoga Postures","Breathing Techniques","Visualization and " +
                "Meditation","Food and Drink"};
    }

    //returns the childExercise strings
    public String[] getChildExercise() {
        return childExercise;
    }

    //sets the childExercise strings based off of the recent value
    public void setChildExercise(int recent) {
        switch (recent) {
            case 0:
                childExercise = new String[]{"Please take an Assessment first"};
                break;
            case 1:
                childExercise = new String[]{"Walking Moderate Pace","Hiking in Nature","Exercise at " +
                        "Sunrise","Tai Chi or Qi Gong","Exercise with Pets or Friends"};
                break;
            case 2:
                childExercise = new String[]{"Weight Lifting","Pilates","Slow Walking","Exercise with a Friend",
                        "Focus Exercise on Lower Body"};
                break;
            case 3:
                childExercise = new String[]{"Swimming","Water Aerobics","Sailing or Kayaking","Surfing",
                        "Never Exercise in Heat"};
                break;
            case 4:
                childExercise = new String[]{"Dancing","Biking","Rowing","Aerobics",
                        "High Intensity Interval Training (HITT)"};
                break;
        }
    }

    //returns the string for ChildYoga
    public String[] getChildYoga() {
        return childYoga;
    }

    //sets the ChildYoga strings based off of the recent value
    public void setChildYoga(int recent) {
        switch (recent) {
            case 0:
                childYoga = new String[]{"Please take an Assessment first"};
                break;
            case 1:
                childYoga = new String[]{"Stright Spine Poses","Balancing Postures"};
                break;
            case 2:
                childYoga = new String[]{"Forward Bends","Hip Openers"};
                break;
            case 3:
                childYoga = new String[]{"Twists","Inversions"};
                break;
            case 4:
                childYoga = new String[]{"Backbends","Lateral Bends"};
                break;
        }
    }

    //returns the string for ChildBreathing
    public String[] getChildBreathing() {
        return childBreathing;
    }

    //sets the ChildBreathing strings based off of the recent value
    public void setChildBreathing(int recent) {
        switch (recent) {
            case 0:
                childBreathing = new String[]{"Please take an Assessment first"};
                break;
            case 1:
                childBreathing = new String[]{"Inhale Equal to Exhale","Alternate Nostril Breathing"
                        ,"Ratio 1.1.1.1"};
                break;
            case 2:
                childBreathing = new String[]{"Long Exhale","Pause After Exhale",
                        "Pause at Top of Inhale and Exhale"};
                break;
            case 3:
                childBreathing = new String[]{"Cooling Breath","6 Second Breathing","Moon Breathing"};
                break;
            case 4:
                childBreathing = new String[]{"Breath of Fire","Pause After Inhale","Sun Breathing"};
                break;
        }
    }

    //returns the string for ChildMeditation
    public String[] getChildMeditation() {
        return childMeditation;
    }

    //sets the ChildMeditation strings based off of the recent value
    public void setChildMeditation(int recent) {
        switch (recent) {
            case 0:
                childMeditation = new String[]{"Please take an Assessment first"};
                break;
            case 1:
                childMeditation = new String[]{"Meditation Audio"};
                break;
            case 2:
                childMeditation = new String[]{"Meditation Audio"};
                break;
            case 3:
                childMeditation = new String[]{"Meditation Audio"};
                break;
            case 4:
                childMeditation = new String[]{"CMeditation Audio"};
                break;
        }
    }

    //returns the string for ChildDiet
    public String[] getChildDiet() {
        return childDiet;
    }

    //sets the ChildDiet strings based off of the recent value
    public void setChildDiet(int recent) {
        switch (recent) {
            case 0:
                childDiet = new String[]{"Please take an Assessment first"};
                break;
            case 1:
                childDiet = new String[]{"Eat plant-based diet","No Snacking Between Meals",
                        "Eat Less Sugar","Eat Less White Flour","Eat Less Dairy"};
                break;
            case 2:
                childDiet = new String[]{"Cut Caffeine In Half","Decrease Cold Drinks",
                        "Eat More Warm Soups","Eat More Often","Eat Heavier Foods"};
                break;
            case 3:
                childDiet = new String[]{"Cut Caffeine In Half","Eat Less Spicy Food",
                        "Eat Less Fried Food","Eat More Fruits","Eat More Veggies"};
                break;
            case 4:
                childDiet = new String[]{"Add Caffeine","Decrease Cold Drinks",
                        "Only Eat When You Are Hungry","Eat Lighter Foods","Avoid Dairy"};
                break;
        }
    }

    //returns the string for SubChildExercise
    public String[] getSubChildExercise() {
        return subChildExercise;
    }

    //sets the SubChildExercise strings based off of the recent value
    public void setSubChildExercise(int recent, int list) {
        switch (recent) {
            case 0:
                subChildExercise = new String[]{"Please take an assessment first"};
            case 1:
                switch (list) {
                    case 0:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 2:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 3:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 4:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildExercise = new String[]{""};
                }
            case 2:
                switch (list) {
                    case 0:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 2:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 3:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 4:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildExercise = new String[]{""};
                }
            case 3:
                switch (list) {
                    case 0:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 2:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 3:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 4:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildExercise = new String[]{""};
                }
            case 4:
                switch (list) {
                    case 0:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 2:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 3:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    case 4:
                        subChildExercise = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildExercise = new String[]{""};
                }
        }
    }

    //returns the string for SubChildYoga
    public String[] getSubChildYoga() {
        return subChildYoga;
    }

    //sets the SubChildYoga strings based off of the recent value
    public void setSubChildYoga(int recent,int list) {
        switch (recent) {
            case 0:
                subChildYoga = new String[]{"Please take an assessment first"};
            case 1:
                switch (list) {
                    case 0:
                        subChildYoga = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildYoga = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildYoga = new String[]{""};
                }
            case 2:
                switch (list) {
                    case 0:
                        subChildYoga = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildYoga = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildYoga = new String[]{""};
                }
            case 3:
                switch (list) {
                    case 0:
                        subChildYoga = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildYoga = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildYoga = new String[]{""};
                }
            case 4:
                switch (list) {
                    case 0:
                        subChildYoga = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildYoga = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildYoga = new String[]{""};
                }
        }
    }

    //returns the string for SubChildBreathing
    public String[] getSubChildBreathing() {
        return subChildBreathing;
    }

    //sets the SubChildBreathing strings based off of the recent value
    public void setSubChildBreathing(int recent,int list) {
        switch (recent) {
            case 1:
            case 0:
                subChildBreathing = new String[]{"Please take an assessment first"};
                switch (list) {
                    case 0:
                        subChildBreathing = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildBreathing = new String[]{"Details coming soon"};
                        break;
                    case 2:
                        subChildBreathing = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildBreathing = new String[]{""};
                }
            case 2:
                switch (list) {
                    case 0:
                        subChildBreathing = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildBreathing = new String[]{"Details coming soon"};
                        break;
                    case 2:
                        subChildBreathing = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildBreathing = new String[]{""};
                }
            case 3:
                switch (list) {
                    case 0:
                        subChildBreathing = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildBreathing = new String[]{"Details coming soon"};
                        break;
                    case 2:
                        subChildBreathing = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildBreathing = new String[]{""};
                }
            case 4:
                switch (list) {
                    case 0:
                        subChildBreathing = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildBreathing = new String[]{"Details coming soon"};
                        break;
                    case 2:
                        subChildBreathing = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildBreathing = new String[]{""};
                }
        }
    }

    //returns the string for SubChildMeditation
    public String[] getSubChildMeditation() {
        return subChildMeditation;
    }

    //sets the SubChildMeditation strings based off of the recent value
    public void setSubChildMeditation(int recent,int list) {
        switch (recent) {
            case 0:
                subChildMeditation = new String[]{"Please take an assessment first"};
            case 1:
                switch (list) {
                    case 0:
                        subChildMeditation = new String[]{"Coming soon"};
                        break;
                    default:
                        subChildMeditation = new String[]{""};
                }
            case 2:
                switch (list) {
                    case 0:
                        subChildMeditation = new String[]{"Coming soon"};
                        break;
                    default:
                        subChildMeditation = new String[]{""};
                }
            case 3:
                switch (list) {
                    case 0:
                        subChildMeditation = new String[]{"Coming soon"};
                        break;
                    default:
                        subChildMeditation = new String[]{""};
                }
            case 4:
                switch (list) {
                    case 0:
                        subChildMeditation = new String[]{"Coming soon"};
                        break;
                    default:
                        subChildMeditation = new String[]{""};
                }
        }
    }

    //returns the string for SubChildDiet
    public String[] getSubChildDiet() {
        return subChildDiet;
    }

    //sets the SubChildDiet strings based off of the recent value
    public void setSubChildDiet(int recent, int list) {
        switch (recent) {
            case 0:
                subChildDiet = new String[]{"Please take an assessment first"};
            case 1:
                switch (list) {
                    case 0:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 2:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 3:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 4:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildDiet = new String[]{""};
                }
            case 2:
                switch (list) {
                    case 0:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 2:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 3:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 4:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildDiet = new String[]{""};
                }
            case 3:
                switch (list) {
                    case 0:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 2:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 3:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 4:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildDiet = new String[]{""};
                }
            case 4:
                switch (list) {
                    case 0:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 1:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 2:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 3:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    case 4:
                        subChildDiet = new String[]{"Details coming soon"};
                        break;
                    default:
                        subChildDiet = new String[]{""};
                }
        }
    }
}
